﻿using System;
using UnityEngine;

namespace Models.Rooms
{
    [Serializable]
    public class SmartLine
    {
        public const float AngleAlphaForCompare = 0.999999f;
        public const int NoContact = -1;
        public const int Angle_Cross = 0;
        public const int Angle_0 = 1;
        public const int Angle_180 = 2;
        public const int Angle_0_2 = 3;
        public const int Angle_180_2 = 4;

        public int ParallelСircuitLine(WallLine wallB)
        {
            if (!Rect.Compere(wallB.Rect))
            {
                return NoContact;
            }

            return CompereAngleScalaris(wallB.A, wallB.B);
        }

        public Vector2 A;
        public Vector2 B;

        public LineRect Rect;

        public SmartLine(Vector2 a, Vector2 b)
        {
            SetAB(a, b);
        }

        public void SetAB(Vector2 a, Vector2 b)
        {
            A = a;
            B = b;
            CalculateRect();
        }

        public void SetA(Vector2 a)
        {
            A = a;
            CalculateRect();
        }

        public void SetB(Vector2 b)
        {
            B = b;
            CalculateRect();
        }

        public void CalculateRect()
        {
            Rect = new LineRect(A.x, A.y, B.x, B.y);
        }

        public int CompereAngleScalaris(Vector2 testA, Vector2 testB)
        {
            Vector2 v1 = (testB - testA).normalized;
            Vector2 v2 = (B - A).normalized;
            Vector2 v3 = (B - testA).normalized;
            var dot1 = v2.x * v1.x + v2.y * v1.y;
            var dot2 = v3.x * v1.x + v3.y * v1.y;
            var delta = AngleAlphaForCompare;
            if (dot1 > delta && dot2 > delta)
                return Angle_0;
            if (dot1 < -delta && dot2 < -delta)
                return Angle_180;
            if (dot1 > delta && dot2 < -delta)
                return Angle_0_2;
            if (dot1 < -delta && dot2 > delta)
                return Angle_180_2;

            return Angle_Cross;
        }

        public bool CompereA(Vector2 point)
        {
            return Mathf.Abs(A.x - point.x) < 0.00001f && Mathf.Abs(A.y - point.y) < 0.00001f;
        }

        public bool CompereB(Vector2 point)
        {
            return Mathf.Abs(B.x - point.x) < 0.00001f && Mathf.Abs(B.y - point.y) < 0.00001f;
        }

        public Vector2 GetLerp05()
        {
            return Vector2.Lerp(A, B, 0.5f);
        }
    }
}