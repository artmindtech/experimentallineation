﻿using System.Runtime.ExceptionServices;
using UnityEngine;

namespace Models.Rooms.Graph
{
    public class TestDeijkstra
    {
        public TestDeijkstra()
        {
            var g = new Graph();

            //добавление вершин
            g.AddVertex(1);
            g.AddVertex(2);
            g.AddVertex(3);
            g.AddVertex(5);
            g.AddVertex(6);
            g.AddVertex(77);
            g.AddVertex(72);

            //добавление ребер
            g.AddEdge(1, 2, 22);
            g.AddEdge(1, 3, 33);
            g.AddEdge(1, 5, 61);
            g.AddEdge(2, 3, 47);
            g.AddEdge(2, 6, 93);
            g.AddEdge(3, 5, 11);
            g.AddEdge(3, 6, 79);
            g.AddEdge(3, 77, 63);
            g.AddEdge(5, 77, 41);
            g.AddEdge(6, 77, 17);
            g.AddEdge(6, 72, 58);
            g.AddEdge(77, 72, 84);

            var dijkstra = new Dijkstra(g);
            var path = dijkstra.FindShortestPath(1, 72);
            Debug.LogWarning($"StartDeijkstra <color=blue>StartDeijkstra</color> path:{path.Path.Length}");
            for (int i = 0; i < path.Path.Length; i++)
            {
                Debug.LogWarning($"StartDeijkstra <color=blue>StartDeijkstra</color> path[i]:{path.Path[i]}");
            }
        }
    }
}