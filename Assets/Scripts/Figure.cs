﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Models.Rooms
{
    /// <summary>
    /// Figure — Looped collection of lines
    /// </summary>
    /// <param name="lines">Many wall lines for closed figure.</param>
    public class Figure
    {
        private static int Index = 0;

        [Serializable]
        private class SeparateDot
        {
            public Figure FigureA;
            public Figure FigureB;
            public List<Vector2> Dots = new List<Vector2>();

            public SeparateDot(Figure figureA, Figure figureB)
            {
                FigureA = figureA;
                FigureB = figureB;
            }
        }

        public List<WallLine> Lines;
        private readonly int _index;
        private bool _subtract;

        public Figure(List<WallLine> lines)
        {
            _index = Index++;
            Lines = lines;
        }

        public bool CollisionWithFigureLine(LineRect line)
        {
            foreach (var point in Lines)
            {
                var collision = new Vector2();
                if (LineRect.CrossLine(line, point.Rect, ref collision))
                {
                    return true;
                }
            }

            return false;
        }

        public bool CollisionWithFigure(float x, float y)
        {
            bool result = false;
            var lineRect = new LineRect(x, y, x + 1000f, y - 1000f);
            foreach (var point in Lines)
            {
                var collision = new Vector2();
                if (LineRect.CrossLine(lineRect, point.Rect, ref collision))
                    result = !result;
            }

            return result;
        }

        public void Subtraction(Figure figure)
        {
            _subtract = true;

            // Разделяем на линии
            SeparateIntersection(figure, out List<WallLine> newLines, out List<WallLine> removeLines);
            
            // Добавляем линии которые полностью вошли в фигуру
            for (int i = 0; i < Lines.Count; i++)
            {
                var line = Lines[i];
                if (figure.CollisionWithFigure(line.A.x, line.A.y) && !figure.CollisionWithFigureLine(line.Rect))
                {
                    line.NeedWall = true;
                    line.Figure = this;
                    newLines.Add(line);
                }
            }

            // Удоляем старые линии которые полностью вошли в нашу фигуру
            for (int i = 0; i < figure.Lines.Count; i++)
            {
                var line = figure.Lines[i];
                if (CollisionWithFigure(line.A.x, line.A.y) && CollisionWithFigure(line.B.x, line.B.y))
                {
                    removeLines.Add(line);
                }
            }

            // Удоляем новые линии центр которых внутри нашей фигуры
            for (int i = 0; i < newLines.Count; i++)
            {
                var center = Vector2.Lerp(newLines[i].A, newLines[i].B, 0.5f);
                if (newLines[i].Figure == figure && CollisionWithFigure(center.x, center.y))
                {
                    newLines.RemoveAt(i);
                    i--;
                    continue;
                }

                if (newLines[i].Figure == this && figure.CollisionWithFigure(center.x, center.y))
                {
                    newLines[i].NeedWall = true;
                }
            }

            // Добавляем линнии в фигуру из которой вычитаем
            foreach (var line in newLines)
            {
                if (line.Figure == null)
                    continue;
                if (line.Figure != this)
                {
                    line.Figure.Lines.Add(line);
                }
                else
                {
                    var newLine = line.Clone();
                    newLine.Flip();
                    figure.Lines.Add(newLine);
                }
            }

            // Удаляем все линии из списка на удаление
            for (int i = 0; i < removeLines.Count; i++)
            {
                figure.Lines.Remove(removeLines[i]);
            }
        }

        public void Join(Figure figure)
        {
            _subtract = false;

            // Разделяем на линии
            SeparateIntersection(figure, out List<WallLine> newLines, out List<WallLine> removeLines);

            // Проверяем линии на вхождение в нашу фигуру
            for (int i = 0; i < Lines.Count; i++)
            {
                var l = Lines[i];
                var center = Vector2.Lerp(l.A, l.B, 0.5f);
                if (figure.CollisionWithFigure(center.x, center.y))
                {
                    removeLines.Add(Lines[i]);
                }
            }

            // Проверяем линии на вхождение в фигуру которую мы присоединяем
            for (int i = 0; i < figure.Lines.Count; i++)
            {
                var l = figure.Lines[i];
                var center = Vector2.Lerp(l.A, l.B, 0.5f);
                if (CollisionWithFigure(center.x, center.y))
                {
                    figure.Lines.RemoveAt(i);
                    i--;
                }
            }

            // Добавляем все линии присоеднияемой фигуры в нашу фигуру
            Lines.AddRange(figure.Lines);

            // Удаляем все линии из списка на удаление
            for (int i = 0; i < removeLines.Count; i++)
            {
                Lines.Remove(removeLines[i]);
            }

            // Добавляем все новые линии
            Lines.AddRange(newLines);
        }

        private void SeparateIntersection(Figure figure, out List<WallLine> newLines, out List<WallLine> removeLines)
        {
            var separateDots = new Dictionary<WallLine, SeparateDot>();
            newLines = new List<WallLine>();
            removeLines = new List<WallLine>();
            //# Проверям поралельные линии, и подвигаем линию стены, что бы не было быга
            CheckParallel(figure);

            //# Смотрим все точки пересечения и фиксируем их
            for (int i = 0; i < Lines.Count; i++)
            {
                var lineA = Lines[i];
                for (int j = 0; j < figure.Lines.Count; j++)
                {
                    var lineB = figure.Lines[j];
                    var collision = new Vector2();
                    if (LineRect.CrossLine(lineA.Rect, lineB.Rect, ref collision))
                    {
                        if (!separateDots.ContainsKey(lineA)) separateDots[lineA] = new SeparateDot(figure, this);
                        separateDots[lineA].Dots.Add(collision);
                        if (!separateDots.ContainsKey(lineB)) separateDots[lineB] = new SeparateDot(this, figure);
                        separateDots[lineB].Dots.Add(collision);
                    }
                }
            }

            //# Разделяем на линии
            SeparateLine(separateDots, newLines, removeLines);
        }

        private void CheckParallel(Figure figure)
        {
            List<WallLine> lines = figure.Lines;
            List<WallLine> tests = new List<WallLine>();
            for (int i = 0; i < Lines.Count; i++)
            {
                var lineA = Lines[i];
                var parallelIndexes = new List<int>();
                var fixedParallel = false;
                for (int j = 0; j < lines.Count; j++)
                {
                    var lineB = lines[j];
                    var variation = lineA.ParallelСircuitLine(lineB);
                    switch (variation)
                    {
                        case SmartLine.Angle_180:
                        case SmartLine.Angle_180_2:
                        case SmartLine.Angle_0_2:
                        case SmartLine.Angle_0:
                            TestFigures.Show(tests);
                            parallelIndexes.Add(j);
                            break;
                    }
                }

                for (int j = 0; j < parallelIndexes.Count; j++)
                {
                    figure.FixedParallel(parallelIndexes[j]);
                    fixedParallel = true;
                }

                if (fixedParallel)
                {
                    i = -1;
                }
            }
        }

        private void FixedParallel(int index)
        {
            var fixedLine = Lines[index];
            WallLine nextLine = GetNextLineByIndex(index);
            WallLine prevLine = GetPrevLineByIndex(index);
            fixedLine.FixedParallel();
            if (nextLine != null)
                nextLine.SetAB(fixedLine.B, nextLine.B);
            if (prevLine != null)
                prevLine.SetAB(prevLine.A, fixedLine.A);
        }

        private WallLine GetPrevLineByIndex(int index)
        {
            var current = Lines[index];
            var prev = index - 1 >= 0 ? Lines[index - 1] : Lines[Lines.Count - 1];
            if (!prev.CompereB(current.A))
            {
                var fail = true;
                for (int i = 0; i < Lines.Count; i++)
                {
                    if (Lines[i].CompereB(current.A))
                    {
                        prev = Lines[i];
                        fail = false;
                        break;
                    }
                }

                if (fail)
                {
                    Debug.LogWarning(
                        $"Figure <color=red>GetPrevLineByIndex</color> Bug With Index {index} current line {current}");
                    return null;
                }
            }


            return prev;
        }

        private WallLine GetNextLineByIndex(int index)
        {
            var current = Lines[index];
            var next = index + 1 < Lines.Count ? Lines[index + 1] : Lines[0];
            if (!next.CompereA(current.B))
            {
                var fail = true;
                for (int i = 0; i < Lines.Count; i++)
                {
                    if (Lines[i].CompereA(current.B))
                    {
                        next = Lines[i];
                        fail = false;
                        break;
                    }
                }

                if (fail)
                {
                    Debug.LogWarning(
                        $"Figure <color=red>GetNextLineByIndex</color> Bug With Index {index} current line {current}");
                    return null;
                }
            }

            return next;
        }

        private void SeparateLine(Dictionary<WallLine, SeparateDot> separateDots, List<WallLine> newLines,
            List<WallLine> removeLines)
        {
            var addLines = new List<WallLine>();
            foreach (var value in separateDots)
            {
                addLines.Clear();
                var line = value.Key;
                var dots = value.Value.Dots;
                var figureA = value.Value.FigureA;
                var figureB = value.Value.FigureB;

                Vector2 a;
                Vector2 b;

                dots.Sort(delegate(Vector2 aa, Vector2 bb)
                {
                    var distanceA = (aa - line.A).magnitude;
                    var distanceB = (bb - line.A).magnitude;
                    if (distanceA > distanceB) return 1;
                    if (distanceA < distanceB) return -1;
                    return 0;
                });

                for (int i = 0; i < dots.Count + 1; i++)
                {
                    if (i == 0)
                    {
                        a = line.A;
                        b = dots[0];
                    }
                    else if (i == dots.Count)
                    {
                        a = dots[i - 1];
                        b = line.B;
                    }
                    else
                    {
                        a = dots[i - 1];
                        b = dots[i];
                    }

                    var wallLine = new WallLine(line.Wall, line.Face, a, b);
                    wallLine.Figure = figureB;
                    addLines.Add(wallLine);
                }

                removeLines.Add(line);

                if (_subtract)
                {
                    for (int i = 0; i < addLines.Count; i++)
                    {
                        var l = addLines[i];
                        var center = Vector2.Lerp(l.A, l.B, 0.5f);
                        if (figureA != this && !figureA.CollisionWithFigure(center.x, center.y))
                        {
                            addLines.RemoveAt(i);
                            i--;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < addLines.Count; i++)
                    {
                        var l = addLines[i];
                        var center = Vector2.Lerp(l.A, l.B, 0.5f);
                        if (figureA.CollisionWithFigure(center.x, center.y))
                        {
                            addLines.RemoveAt(i);
                            i--;
                        }
                    }
                }

                newLines.AddRange(addLines);
            }
        }

        // public override string ToString()
        // {
        //     string walls = "";
        //     for (int i = 0; i < Lines.Count; i++)
        //     {
        //         walls += Lines[i].Wall.guid.Substring(0, 4) + "_" + Lines[i].FaceString + " ";
        //     }
        //
        //     return $"Figure[{Lines.Count}] Walls:{walls}";
        // }

        //
        // public static Figure[] Сombine(List<Figure> figures)
        // {
        //     var results = new List<Figure>();
        //     var readyCombine = new List<int>();
        //     for (int i = 0; i < figures.Count; i++)
        //     {
        //         if (readyCombine.Contains(i))
        //             continue;
        //         var f1 = figures[i];
        //         
        //         for (int j = 0; j < figures.Count; j++)
        //         {
        //             if (i == j || readyCombine.Contains(j))
        //                 continue;
        //             var f2 = figures[j];
        //             if (f1.TrySeparateBy(f2))
        //             {
        //                 readyCombine.Add(i);
        //                 readyCombine.Add(j);
        //             }
        //         }
        //     }
        //
        //     return results.ToArray();
        // }
        //
        // struct SeparateResultLinis
        // {
        //     public List<WallLine> Lines;
        //     public Figure Separator;
        // }
        //
        // private List<SeparateResultLinis> _separates = new List<SeparateResultLinis>();
        //
        // private bool TrySeparateBy(Figure otherFigure)
        // {
        //     var result = false;
        //     var lines = new List<WallLine>();
        //     for (int i = 0; i < Lines.Count; i++)
        //     {
        //         var main = Lines[i];
        //         for (int j = 0; j < otherFigure.Lines.Count; j++)
        //         {
        //             var other = otherFigure.Lines[j];
        //             if (main.Rect.Compere(other.Rect))
        //             {
        //                 Vector2 dot = new Vector2();
        //                 if (RoomsDetector.CrossLine(main.Rect, other.Rect, ref dot))
        //                 {
        //                     var newLines = new List<WallLine>();
        //                     _separates.Add(new SeparateResultLinis
        //                         {Separator = otherFigure, Lines = newLines});
        //                     result = true;
        //                 }
        //             }
        //             else
        //             {
        //                 lines
        //             }
        //         }
        //     }
        //     return result;
        // }
        public int GetIndex()
        {
            return _index;
        }
    }
}