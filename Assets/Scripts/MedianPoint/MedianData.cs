﻿using System.Collections.Generic;
using System.Linq;
using Models.Rooms.Graph;
using UnityEngine;

namespace Models.Rooms.MedianPoint
{
    public class MedianData
    {
        public struct AngleDiagonalLine
        {
            public LineRect Line;
            public int Type;
        }

        public List<AngleDiagonalLine> DrawAngleDiagonal = new List<AngleDiagonalLine>();
        private readonly List<AngleRay> _rays = new List<AngleRay>();
        private readonly List<int> _raysConcave = new List<int>();
        private List<LineEdge> _skeleton;

        private Dictionary<int, List<LineEdge>> _nodes;

        private List<PositionsPath> _paths;
        private List<Vector2> _medianDot = new List<Vector2>();

        public void Build(float minRay, float minConcave, float combiningDistance)
        {
            // Замыкаем проверку пересечения соседних лучей вогнутых углов
            if (HasConcaveCount())
            {
                var start = GetConcaveFirst();
                var end = GetConcaveLast();
                Vector2 crossPoint = Vector2.zero;
                var startRay = _rays[start];
                var endRay = _rays[end];
                if (LineRect.CrossLine(startRay.Ray, endRay.Ray, ref crossPoint))
                {
                    var endIndex = _rays.IndexOf(endRay);
                    startRay.AddCrossPoint(crossPoint);
                    endRay.AddCrossPoint(crossPoint);
                    _rays[start] = startRay;
                    _rays[endIndex] = endRay;
                }
            }

            // Удаление невалидных точек
            for (int i = 0; i < _rays.Count; i++)
            {
                var currentRay = _rays[i];

                // Если не имеет точек (вероятно обход не вту сторону)
                if (currentRay.NoPoint)
                {
                    RemoveRayByIndex(i);
                    i--;
                    continue;
                }

                // Если слишком билзко к началу луча
                if ((_rays[i].CrossPoint - _rays[i].Start).sqrMagnitude < Mathf.Sqrt(minRay))
                {
                    RemoveRayByIndex(i);
                    i--;
                    continue;
                }

                _medianDot.Clear();
                for (int j = 0; j < _rays.Count; j++)
                {
                    if (i == j) continue;
                    var nextRay = _rays[j];

                    // Если слишком близко к выпуклым углам
                    if (_raysConcave.IndexOf(j) == -1)
                    {
                        var from = currentRay.Start;
                        var to = currentRay.CrossPoint;
                        var point = nextRay.Start;
                        //
                        var normalize = (from - to).normalized;
                        var lp = point - from;
                        var result = normalize * Vector2.Dot(normalize, lp);

                        LineRect testLine = new LineRect(point.x, point.y, from.x, from.y);
                        DrawAngleDiagonal.Add(new AngleDiagonalLine { Line = testLine, Type = 2 });
                        Debug.LogWarning($"MedianData <color=blue>Build</color> {i} {j} add:{testLine}");
                        // TestMedianPoints.DrawTarget = result;
                        // var magnitude = (result - nextRay.Start).magnitude;
                        // Debug.LogWarning($"MedianData <color=blue>Build</color> result:{result} {nextRay.Start} {currentRay.CrossPoint}");
                        
                        var magnitude = (currentRay.CrossPoint - nextRay.Start).magnitude;
                        if (magnitude < minConcave)
                        {
                            _medianDot.Add(Vector2.Lerp(currentRay.Start, nextRay.Start, 0.5f));
                            RemoveRayByIndex(i);
                            i--;
                            break;
                        }
                    }

                    // Если слишком билзко к другой точке, нас удаляем и сдвигаем другую в нашу сторону
                    var gap = currentRay.CrossPoint - nextRay.CrossPoint;
                    if (gap.magnitude < combiningDistance)
                    {
                        nextRay.MoveToGap(gap);
                        _rays[j] = nextRay;
                        RemoveRayByIndex(i);
                        i--;
                        break;
                    }
                }
            }

            if (_rays.Count > 1)
                BuildEdges();
        }

        private void BuildEdges()
        {
            var points = GetPoints();
            List<List<int>> g = new List<List<int>>();
            for (int i = 0; i < points.Length; i++)
            {
                g.Add(new List<int>());
                for (int j = 0; j < points.Length; j++)
                {
                    g[i].Add((int)((points[i] - points[j]).sqrMagnitude * 1000f));
                }
            }

            // Алгоритм прима
            const int INF = 1000000000;
            _skeleton = new List<LineEdge>();
            _nodes = new Dictionary<int, List<LineEdge>>();
            _paths = new List<PositionsPath>();
            int n = points.Length;
            var used = new bool[n];
            var min_e = new int[n];
            var sel_e = new int[n];
            for (int i = 0; i < n; i++)
            {
                min_e[i] = INF;
                sel_e[i] = -1;
            }

            min_e[0] = 0;
            var graph = new Graph.Graph();
            // Добавляем нулевую точку в граф
            graph.AddVertex(0);
            for (int i = 0; i < n; i++)
            {
                int v = -1;
                for (int j = 0; j < n; j++)
                    if (!used[j] && (v == -1 || min_e[j] < min_e[v]))
                        v = j;
                if (min_e[v] == INF)
                    break;

                used[v] = true;
                var aId = sel_e[v];
                var bId = v;
                if (aId != -1)
                {
                    var a = points[aId];
                    var b = points[bId];
                    var lineEdge = new LineEdge(aId, bId, a, b, g[aId][bId]);

                    if (!_nodes.ContainsKey(aId))
                    {
                        _nodes[aId] = new List<LineEdge>();
                    }

                    // Добавляем точки в граф
                    graph.AddVertex(bId);

                    _nodes[aId].Add(lineEdge);
                    _skeleton.Add(lineEdge);
                }

                for (int to = 0; to < n; to++)
                    if (g[v][to] < min_e[to])
                    {
                        min_e[to] = g[v][to];
                        sel_e[to] = v;
                    }
            }

            var startNodes = new List<int>();

            foreach (var id in _nodes.Keys)
            {
                for (int i = 0; i < _nodes[id].Count; i++)
                {
                    if (startNodes.IndexOf(id) != -1)
                        startNodes.Remove(id);
                    // Добавление ребер
                    graph.AddEdge(id, _nodes[id][i].Idb, _nodes[id][i].Length);
                    startNodes.Add(_nodes[id][i].Idb);
                }
            }

            // Проверка на количество выходящих ребер
            if (_nodes[0].Count < 2)
            {
                // Добавляем нулевую позицию как один из вариантов начала пути
                startNodes.Add(0);
            }

            var dijkstra = new Dijkstra(graph);
            for (int j = 0; j < startNodes.Count - 1; j++)
            {
                var start = startNodes[j];
                for (int k = j + 1; k < startNodes.Count; k++)
                {
                    var end = startNodes[k];
                    var path = dijkstra.FindShortestPath(start, end);

                    List<Vector2> positions = new List<Vector2>();
                    var pathLength = 0f;
                    for (int i = 0; i < path.Path.Length; i++)
                    {
                        var a = path.Path[i];
                        var aPosition = points[a];
                        if (i + 1 < path.Path.Length)
                        {
                            var bPosition = points[path.Path[i + 1]];
                            pathLength += (bPosition - aPosition).magnitude;
                        }

                        // Добавление позиции
                        positions.Add(aPosition);
                        // _pathTest.Add(p);
                    }

                    var positionsPath = new PositionsPath
                    {
                        Positions = positions.ToArray(),
                        Length = pathLength,
                        Path = path.Path
                    };
                    _paths.Add(positionsPath);
                }
            }

            _paths.Sort((a, b) =>
            {
                if (a.Length > b.Length) return -1;
                if (a.Length < b.Length) return 1;
                return 0;
            });

            // Debug.LogWarning($"MedianData <color=blue>BuildEdges</color> _paths.Count:{_paths.Count}");
            // foreach (var path in _paths)
            // {
            //     Debug.LogWarning(
            //         $"MedianData <color=blue>BuildEdges</color> path.Length:{path.Length} {path.Path[0]} to {path.Path[path.Path.Length-1]}");
            // }
        }

        private void RemoveRayByIndex(int i)
        {
            var indexConcave = _raysConcave.IndexOf(i);
            if (indexConcave != -1)
            {
                _raysConcave.RemoveAt(indexConcave);
            }

            for (int j = 0; j < _raysConcave.Count; j++)
            {
                if (_raysConcave[j] > i)
                    _raysConcave[j]--;
            }

            _rays.RemoveAt(i);
        }

        private bool HasConcaveCount()
        {
            return _raysConcave.Count > 0;
        }

        public Vector2[] GetPoints()
        {
            var dots = _rays.Select(p => p.CrossPoint).ToList();
            foreach (var dot in _medianDot)
            {
                dots.Add(dot);
            }

            return dots.ToArray();
        }

        private int GetConcaveFirst()
        {
            return _raysConcave[0];
        }

        private int GetConcaveLast()
        {
            return _raysConcave[_raysConcave.Count - 1];
        }

        public void Add(AngleRay angleRay, bool concave = false)
        {
            if (concave)
            {
                // Для вогнутых углов проводим проверку пересечения с соседними углами
                if (HasConcaveCount())
                {
                    var prev = GetConcaveLast();
                    var prevRay = _rays[prev];
                    Vector2 crossPoint = Vector2.zero;
                    if (LineRect.CrossLine(prevRay.Ray, angleRay.Ray, ref crossPoint))
                    {
                        prevRay.AddCrossPoint(crossPoint);
                        angleRay.AddCrossPoint(crossPoint);
                        _rays[prev] = prevRay;
                    }
                }

                _raysConcave.Add(_rays.Count);
            }
            _rays.Add(angleRay);
            DrawAngleDiagonal.Add(new AngleDiagonalLine { Line = angleRay.Ray, Type = concave ? 0 : 1 });
        }

        public List<LineEdge> GetSkeleton()
        {
            return _skeleton;
        }

        public List<PositionsPath> GetPaths()
        {
            return _paths;
        }
    }

    public struct LineEdge
    {
        public int Ida { get; }
        public int Idb { get; }
        public int Length { get; }
        public Vector2 A { get; }
        public Vector2 B { get; }

        public LineEdge(int idA, int idB, Vector2 a, Vector2 b, int length)
        {
            Ida = idA;
            Idb = idB;
            A = a;
            B = b;
            Length = length;
        }
    }
}