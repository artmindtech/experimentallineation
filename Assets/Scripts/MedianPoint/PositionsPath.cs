﻿using System;
using UnityEngine;

namespace Models.Rooms.MedianPoint
{
    public struct PositionsPath
    {
        public float Length;
        public Vector2[] Positions;
        public int[] Path { get; set; }

        public Vector2 GetPathPosition(float t)
        {
            var positionOnPath = t * Length;
            var i = 0;
            var current = (Positions[i + 1] - Positions[i]).magnitude;
            var position = Positions[0];
            Debug.DrawLine(new Vector3(position.x, 0.1f, position.y), new Vector3(position.x, -0.1f, position.y));
            while (i < Positions.Length - 1 && current < positionOnPath)
            {
                position = Positions[++i];
                Debug.DrawLine(new Vector3(position.x, 0.1f, position.y), new Vector3(position.x, -0.1f, position.y));
                var add = (Positions[i + 1] - position).magnitude;
                current += add;
            }

            var percent = Math.Abs(positionOnPath - current) / (Positions[i + 1] - Positions[i]).magnitude;
            return Vector2.Lerp(Positions[i], Positions[i + 1], 1f - percent);
        }
    }
}