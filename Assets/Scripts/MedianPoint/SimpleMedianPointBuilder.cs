﻿using System.Collections.Generic;
using UnityEngine;

namespace Models.Rooms.MedianPoint
{
    public class SimpleMedianPointBuilder
    {
        public List<MedianData.AngleDiagonalLine> DrawAngleDiagonal = new List<MedianData.AngleDiagonalLine>();
        /// <summary>
        /// Класс расчитывает середину полигона, возвращаяет сколько источников света нужно, и где середина полигона.
        /// </summary>
        /// <param name="lines"> Замкнуты контур из линий</param>
        /// <param name="angles"></param>
        /// <param name="curvedAngles"> Добавляет в алгоритм точки от выпуклых углов</param>
        /// <returns>MedianData</returns>
        public MedianData BuildMedianPoints(List<LineRect> lines, MedianSettings settings)
        {
            if (lines.Count > settings.LiniationLimit)
            {
                throw new UnityException($"Liniation In Expansive! Limits {settings.LiniationLimit} liniation.");
            }
            
            // Approximation
            // for (int i = 0; i < lines.Count; i++)
            // {
            //     var current = lines[i];
            //     if (current.Length < settings.MinApproximation)
            //     {
            //         int prevIndex;
            //         int nextIndex;
            //         if (i - 1 > 0)
            //             prevIndex = i - 1;
            //         else
            //             prevIndex = lines.Count - 1;
            //         if (i + 1 < lines.Count)
            //             nextIndex = i + 1;
            //         else
            //             nextIndex = 0;
            //         var prev = lines[prevIndex];
            //         prev.SetA(lines[nextIndex].B);
            //         lines[prevIndex] = prev;
            //         lines.RemoveAt(i);
            //         i--;
            //     }
            // }
            DrawAngleDiagonal.Clear();
            var median = new MedianData();
            for (int i = 0; i < lines.Count; i++)
            {
                var current = lines[i];
                // var next = i - 1 >= 0 ? lines[i - 1] : lines[lines.Count - 1];
                var next = i + 1 < lines.Count ? lines[i + 1] : lines[0];
                
                // Берем углы линий
                var currentAngle = current.Angle;
                var nextAngle = next.Angle;

                // Берем разность углов
                var diffAngle = nextAngle - currentAngle;
                if (diffAngle > Mathf.PI)
                    diffAngle -= Mathf.PI * 2;
                else if (diffAngle < -Mathf.PI)
                    diffAngle += Mathf.PI * 2;
                
                // Проверяем вогнутые углы
                if (settings.ConcaveAngles && diffAngle < -settings.CriticalAngle)
                {
                    var concaveRay = new AngleRay(current.A, GetRayAngle(currentAngle, nextAngle, 0));
                    concaveRay.TryCollision(lines);
                    median.Add(concaveRay, true);
                }
                // Проверяем выгнутые углы
                else if (settings.CurvedAngles && diffAngle > settings.CriticalAngle)
                {
                    if (current.Length > settings.MinLine && next.Length > settings.MinLine)
                    {
                        var curvedRay = new AngleRay(current.A, currentAngle - Mathf.PI + Mathf.PI / 2);
                        curvedRay.TryCollision(lines);
                        median.Add(curvedRay);
                        curvedRay = new AngleRay(current.A, nextAngle - Mathf.PI + Mathf.PI / 2);
                        curvedRay.TryCollision(lines);
                        median.Add(curvedRay);
                    }
                }
            }

            median.Build(settings.MinRay, settings.MinConcave, settings.CombiningDistance);

            return median;
        }

        private static float GetRayAngle(float currentAngle, float nextAngle, int type)
        {
            var finishAngle = type == 0
                ? nextAngle > 0 && currentAngle < 0 ? nextAngle + Mathf.PI : nextAngle - Mathf.PI
                : nextAngle < 0 && currentAngle > 0
                    ? nextAngle - Mathf.PI
                    : nextAngle + Mathf.PI;
            var rayAngle = Mathf.Lerp(finishAngle, currentAngle, 0.5f);
            return rayAngle;
        }
    }

    public struct AngleRay
    {
        private readonly LineRect _ray;
        private Vector2 _crossPoint;
        private readonly Vector2 _startCrossPoint;
        public Vector2 Start { get; }
        public LineRect Ray => _ray;
        public bool NoPoint => _startCrossPoint == _crossPoint;
        public Vector2 CrossPoint => _crossPoint;

        public AngleRay(Vector2 start, float angle, float length = 20f)
        {
            Start = start;
            float xx = Start.x + Mathf.Cos(angle) * length;
            float yy = Start.y + Mathf.Sin(angle) * length;
            _ray = new LineRect(Start.x, Start.y, xx, yy);
            _startCrossPoint = Start + Vector2.one * 1000f;
            _crossPoint = _startCrossPoint;
        }

        public void AddCrossPoint(Vector2 point)
        {
            if ((Start - _crossPoint).sqrMagnitude > (Start - point).sqrMagnitude)
                _crossPoint = point;
        }

        public void TryCollision(List<LineRect> lines)
        {
            foreach (var l in lines)
            {
                Vector2 crossPoint = Vector2.zero;
                if (LineRect.CrossLine(l, Ray, ref crossPoint))
                {
                    AddCrossPoint(Vector2.Lerp(crossPoint, Start, 0.5f));
                }
            }
        }

        public override string ToString()
        {
            return $"RayAngle(start:{Start} crossPoint:{_crossPoint})";
        }

        public void MoveToGap(Vector2 gap)
        {
            _crossPoint = Vector2.Lerp(_crossPoint, _crossPoint + gap, 0.5f);
        }
    }
}