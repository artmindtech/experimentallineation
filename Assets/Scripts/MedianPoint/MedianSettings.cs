using UnityEngine;

namespace Models.Rooms.MedianPoint
{
    [System.Serializable]
    public class MedianSettings
    {
        public bool CurvedAngles;
        public bool ConcaveAngles;
        public float CombiningDistance = 0.5f;
        public float MinConcave = 0.5f;
        public float MinRay = 0.5f;
        public float MinLine = 0.5f;
        public float MinApproximation = 0.2f;
        public int LiniationLimit = 1000;
        public float ActionAngle = 30;
        [HideInInspector] public float CriticalAngle => ActionAngle / Mathf.Rad2Deg;
    }
}