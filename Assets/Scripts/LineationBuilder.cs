﻿using System.Collections.Generic;
using UnityEngine;

namespace Models.Rooms
{
    public class LineationBuilder
    {
        private const int MaxCountLinieation = 1000;

        public List<Lineation> Build(List<WallLine> lines)
        {
            List<Lineation> result = new List<Lineation>();
            var allLines = new List<LineationLine>();
            for (int i = 0; i < lines.Count; i++)
            {
                allLines.Add(new LineationLine(lines[i]));
            }

            SetupLines(ref allLines);

            //# Создаем контуры
            var c = 0;
            while (allLines.Count > 0 && c < MaxCountLinieation)
            {
                c++;
                var lineation = new Lineation(allLines[0]);
                allLines.RemoveAt(0);
                lineation.Searching(ref allLines, c);
                if (!lineation.InComplete())
                {
                    Debug.LogWarning($"LineationBuilder <color=red>Bug Liniation!</color> lineation:{lineation.Lines[0]} and {lineation.Lines[lineation.Lines.Count - 1]}");
                }
                else
                {
                    result.Add(lineation);
                }
            }

            return result;
        }

        private void SetupLines(ref List<LineationLine> lines)
        {
            List<WallLine> hasPrevious = new List<WallLine>();
            var removes = new List<LineationLine>();
            for (int i = 0; i < lines.Count; i++)
            {
                if (lines[i].Line.CompereA(lines[i].B))
                {
                    removes.Add(lines[i]);
                    continue;
                }

                for (int j = 0; j < lines.Count; j++)
                {

                    if (i == j)
                        continue;
                    
                    if (lines[i].Line.CompereA(lines[j].B))
                    {
                        if (lines[i].Line.CompereB(lines[j].A))
                        {
                            removes.Add(lines[i]);
                            removes.Add(lines[j]);
                            lines[i].Why = $"Double Back To {lines[j]}";
                            lines[j].Why = $"Double Back To {lines[i]}";
                        }
                        else
                        {
                            hasPrevious.Add(lines[i].Line);
                            lines[i].AddPrevious(lines[j]);
                        }
                    }
                }

                if (!lines[i].HasPrevious())
                {
                    removes.Add(lines[i]);
                    lines[i].Why = $"No Previous...\n";
                }
            }
            
            for (int i = 0; i < removes.Count; i++)
            {
                removes[i].RemoveNext(removes);
                lines.Remove(removes[i]);
            }

            for (int i = 0; i < lines.Count; i++)
            {
                var remove = false;
                if (!lines[i].HasPrevious())
                {
                    remove = true;
                }
                else if (!lines[i].HasNext())
                {
                    lines[i].ClearPrevious();
                    remove = true;
                }

                if (remove)
                {
                    lines.RemoveAt(i);
                    i = 0;
                }
            }
        }
    }
}