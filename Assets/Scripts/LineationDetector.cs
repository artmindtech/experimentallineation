﻿using System.Collections.Generic;
using UnityEngine;

namespace Models.Rooms
{
    public class LineationDetector
    {
        private static readonly LineationBuilder _lineationBuilder = new LineationBuilder();

        // public TopWallsUpAndDown TopWallsByWalls(List<Wall> wallsTop, List<Wall> wallsDown)
        // {
        //     var topFigure = MakeFigure(MakeLines(wallsTop));
        //     var downFigure = MakeFigure(MakeLines(wallsDown));
        //
        //     topFigure.Subtraction(downFigure);
        //
        //     Debug.LogWarning($"LineationDetector <color=blue>TopWallsByWalls</color> topFigure:{topFigure.Lines.Count} downFigure:{downFigure.Lines.Count}");
        //
        //     var debugLines = new List<WallLine>();
        //     foreach (var line in downFigure.Lines)
        //     {
        //         debugLines.Add(line);
        //         
        //     }
        //     RoomsDetector.DebugWallLines.Clear();
        //     RoomsDetector.DebugWallLines.Add(debugLines);
        //
        // List<Lineation> top = DetectPolygonsByLines(topFigure.Lines);
        // List<Lineation> down = DetectPolygonsByLines(downFigure.Lines);
        //
        //     var t = "top liniation: ";
        //     foreach (var lineation in top)
        //     {
        //         t += $"[count:{lineation.Lines.Count} cilds:{lineation.Childs.Count}] ";
        //     }
        //     var d = "down liniation: ";
        //     foreach (var lineation in down)
        //     {
        //         d += $"[count:{lineation.Lines.Count} cilds:{lineation.Childs.Count}] ";
        //     }
        //     Debug.LogWarning($"LineationDetector <color=blue>TopWallsByWalls</color> top:{top.Count} -> {t} down:{down.Count}  -> {d}");
        //
        //     return new TopWallsUpAndDown { Top = top, Down = down };
        // }

        public static List<Lineation> DetectPolygonsByLines(List<WallLine> lines)
        {
            var lineations = _lineationBuilder.Build(lines);
            for (int i = 0; i < lineations.Count - 1; i++)
            {
                for (int j = 0; j < lineations.Count; j++)
                {
                    if (i == j) continue;
                    var other = lineations[j];
                    if (lineations[i].TrySetChild(other))
                    {
                        lineations.RemoveAt(j);
                        j--;
                    }
                }
            }

            return lineations;
        }

        // public List<Lineation> DetectLineationsByWalls(List<Wall> walls)
        // {
        //     var figure = MakeFigure(MakeLines(walls));
        //     return _lineationBuilder.Build(figure.Lines);
        // }
        //
        // private Figure MakeFigure(List<Figure> _wallsFigures)
        // {
        //     var c = 0;
        //     var figure = _wallsFigures[0];
        //     for (int j = 0; c < RoomsDetector.MaxCountWalls && j < _wallsFigures.Count; j++)
        //     {
        //         c++;
        //         if (j == 0)
        //             continue;
        //         var figureB = _wallsFigures[j];
        //         figure.Join(figureB);
        //         _wallsFigures.Remove(figureB);
        //         j--;
        //     }
        //
        //     return figure;
        // }
        //
        // public static bool CrossLine(LineRect a, LineRect b, ref Vector2 result)
        // {
        //     var p1 = new Vector2(a.Ax, a.Ay);
        //     var p2 = new Vector2(a.Bx, a.By);
        //     var p3 = new Vector2(b.Ax, b.Ay);
        //     var p4 = new Vector2(b.Bx, b.By);
        //     if (AreCrossing(p1, p2, p3, p4))
        //     {
        //         float a1, b1, c1, a2, b2, c2;
        //         a1 = p2.y - p1.y;
        //         b1 = p1.x - p2.x;
        //         c1 = -p1.x * (p2.y - p1.y) + p1.y * (p2.x - p1.x);
        //         a2 = p4.y - p3.y;
        //         b2 = p3.x - p4.x;
        //         c2 = -p3.x * (p4.y - p3.y) + p3.y * (p4.x - p3.x);
        //         result = CrossingPoint(a1, b1, c1, a2, b2, c2);
        //         return true;
        //     }
        //
        //     return false;
        // }

        // private static float VectorMult(float ax, float ay, float bx, float by) //векторное произведение
        // {
        //     return ax * @by - bx * ay;
        // }
        //
        // private static bool AreCrossing(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) //проверка пересечения
        // {
        //     float v1 = VectorMult(p4.x - p3.x, p4.y - p3.y, p1.x - p3.x, p1.y - p3.y);
        //     float v2 = VectorMult(p4.x - p3.x, p4.y - p3.y, p2.x - p3.x, p2.y - p3.y);
        //     float v3 = VectorMult(p2.x - p1.x, p2.y - p1.y, p3.x - p1.x, p3.y - p1.y);
        //     float v4 = VectorMult(p2.x - p1.x, p2.y - p1.y, p4.x - p1.x, p4.y - p1.y);
        //     if (v1 * v2 < 0f && v3 * v4 < 0f)
        //         return true;
        //     return false;
        // }
        //
        // private static Vector2 CrossingPoint(float a1, float b1, float c1, float a2, float b2, float c2)
        // {
        //     Vector2 pt = new Vector2();
        //     float d = a1 * b2 - b1 * a2;
        //     float dx = -c1 * b2 + b1 * c2;
        //     float dy = -a1 * c2 + c1 * a2;
        //     pt.x = dx / d;
        //     pt.y = dy / d;
        //     return pt;
        // }
        //
        // private List<Figure> MakeLines(List<Wall> walls)
        // {
        //     RoomsDetector.DebugWallLines.Clear();
        //     var figures = new List<Figure>();
        //     figures.Clear();
        //     for (int i = 0; i < walls.Count; i++)
        //     {
        //         var wall = walls[i];
        //         var a = ToUnityPoint(wall.From.Get());
        //         var b = ToUnityPoint(wall.To.Get());
        //
        //         var magnitude = (a - b).magnitude;
        //         var p = RoomsDetector.WallLengthOffset / magnitude;
        //         var d = b - a;
        //         b += d * p;
        //         a -= d * p;
        //
        //         var lines = new List<WallLine>();
        //         var length = wall.Thickness / magnitude;
        //         var pointA = new Vector2(d.y, -d.x) * length / 2 + a;
        //         var pointB = a - (pointA - a);
        //         var pointC = pointB - a + b;
        //         var pointD = pointA - a + b;
        //
        //         // Face 0 — ребро со стороны from
        //         lines.Add(new WallLine(wall, 0, pointA, pointB));
        //         // Face 1 — ребро from -> to по часовой стрелке
        //         lines.Add(new WallLine(wall, 1, pointB, pointC));
        //         // Face 2 — ребро to 
        //         lines.Add(new WallLine(wall, 2, pointC, pointD));
        //         // Face 3 — ребро to -> from по часовой стрелке
        //         lines.Add(new WallLine(wall, 3, pointD, pointA));
        //
        //         // Назначаем данные о принадлежности стены комнатам
        //         wall.Faces.Get().SetFaces(lines);
        //         var figure = new Figure(lines);
        //         figures.Add(figure);
        //     }
        //
        //     return figures;
        // }

        private Vector2 ToUnityPoint(System.Numerics.Vector2 data)
        {
            return new Vector2(data.X, data.Y);
        }
    }

    public struct TopWallsUpAndDown
    {
        public List<Lineation> Top;
        public List<Lineation> Down;
    }
}