using UnityEngine;

public struct FigureLine
{
    public Vector2 A { get; }
    public Vector2 B { get; }
    public int Figure { get; }
    public IWallInformation Information { get; }

    public FigureLine(int figure, IWallInformation information, Vector2 a, Vector2 b)
    {
        Figure = figure;
        Information = information;
        A = a;
        B = b;
    }

    public static bool CrossLine(FigureLine a, FigureLine b, out Vector2 result)
    {
        return CrossLine( a.A,  a.B, b.A, b.B, out result);
    }

    public static bool CrossLine(Vector2 p1, Vector2 p2,Vector2 p3,Vector2 p4, out Vector2 result)
    {
        if (AreCrossing(p1, p2, p3, p4))
        {
            float a1, b1, c1, a2, b2, c2;
            a1 = p2.y - p1.y;
            b1 = p1.x - p2.x;
            c1 = -p1.x * (p2.y - p1.y) + p1.y * (p2.x - p1.x);
            a2 = p4.y - p3.y;
            b2 = p3.x - p4.x;
            c2 = -p3.x * (p4.y - p3.y) + p3.y * (p4.x - p3.x);
            result = CrossingPoint(a1, b1, c1, a2, b2, c2);
            return true;
        }
        result = default;
        return false;
    }

    private static float VectorMult(float ax, float ay, float bx, float by) // векторное произведение
    {
        return ax * by - bx * ay;
    }

    private static bool AreCrossing(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) // проверка пересечения
    {
        float v1 = VectorMult(p4.x - p3.x, p4.y - p3.y, p1.x - p3.x, p1.y - p3.y);
        float v2 = VectorMult(p4.x - p3.x, p4.y - p3.y, p2.x - p3.x, p2.y - p3.y);
        float v3 = VectorMult(p2.x - p1.x, p2.y - p1.y, p3.x - p1.x, p3.y - p1.y);
        float v4 = VectorMult(p2.x - p1.x, p2.y - p1.y, p4.x - p1.x, p4.y - p1.y);
        if (v1 * v2 < 0f && v3 * v4 < 0f)
            return true;
        return false;
    }

    private static Vector2 CrossingPoint(float a1, float b1, float c1, float a2, float b2, float c2)
    {
        Vector2 pt = new Vector2();
        float d = a1 * b2 - b1 * a2;
        float dx = -c1 * b2 + b1 * c2;
        float dy = -a1 * c2 + c1 * a2;
        pt.x = dx / d;
        pt.y = dy / d;
        return pt;
    }
}