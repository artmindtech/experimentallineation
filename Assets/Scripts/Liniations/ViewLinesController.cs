﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ViewLinesController : MonoBehaviour
{
    public static readonly Vector2 CompareRayLength = new Vector2(1000f, 1000f);
    public const float WallLengthOffset = 0.005f; // 50 mm
    public bool Check = true;
    public WallData[] Walls;
    public List<LineView> LinesView = new List<LineView>();
    public List<PointView> PointView = new List<PointView>();
    public Vector2 TestRayFrom;
    public Vector2 TestRayTo;

    private void OnValidate()
    {
        LinesView.Clear();
        PointView.Clear();

        if (!Check)
            return;

        var figures = MakeLines(Walls.ToList());
        var points = MakePoints(figures);

        for (int i = 0; i < figures.Count; i++)
        {
            for (var j = 0; j < figures[i].Points.Length; j++)
            {
                var line = figures[i].GetLine(j);
                LinesView.Add(new LineView(i, line.A, line.B, Color.cyan, line.Information));
            }
        }

        for (int i = 0; i < points.Count; i++)
        {
            PointView.Add(new PointView(i, points[i]));
        }
    }

    // private FigureRay MakeRay(Figure3 figure)
    // {
    //     LinesView.Add(new LineView(0, TestRayFrom, TestRayTo, Color.magenta));
    //     return new FigureRay(TestRayFrom, TestRayTo, figure);
    // }

    private List<Vector2> MakePoints(List<Figure3> figures)
    {
        var result = new List<Vector2>();
        for (var i = 0; i < figures.Count; i++)
        {
            var figureA = figures[i];
            for (var j = i + 1; j < figures.Count; j++)
            {
                var figureB = figures[j];
                for (int lineA = 0; lineA < figureA.LineCount; lineA++)
                {
                    for (int lineB = 0; lineB < figureB.LineCount; lineB++)
                    {
                        if (FigureLine.CrossLine(figureA.GetLine(lineA), figureB.GetLine(lineB), out Vector2 crossPoint))
                        {
                            figureA.AddCross(lineA, figureB, lineB, crossPoint);
                            figureB.AddCross(lineB, figureA, lineA, crossPoint);
                            // result.Add(crossPoint);
                            // Добавляем и удаляем линии
                        }
                    }
                }
            }
            Debug.LogWarning($"ViewLinesController <color=cyan>MakePoints</color> figure:{figureA.Index}");
        }
        // Соединяем и составляем новые фигуры
        Debug.LogWarning($"ViewLinesController <color=cyan>MakePoints</color> start creation figures");
        var newFigures = new List<Figure3>();
        for (var i = 0; i < 1; i++)
        // for (var i = 0; i < figures.Count; i++)
        {
            var points = new List<LinePoint>();
            var figure = figures[i];
            for (int j = 0; j < figure.LineCount; j++)
            {
                var point = figure.Points[j];

                var compare = figure.CrossFiguresCompare(point.Position);
                
                // Исключаем из-за вхождения точки в одну из пересекаемых фигур
                if (point.Cross.Count == 0 && compare)
                {
                    Debug.LogWarning($"ViewLinesController <color=cyan>MakePoints</color> j:{j} {point.Position}");
                    continue;
                }
                
                var lineA = figure.GetLine(j);
                
                if (!compare)
                {
                    Debug.LogWarning($"ViewLinesController <color=cyan>MakePoints</color> j:{j}");
                    points.Add(new LinePoint(lineA.Information, lineA.A));
                }
                else
                {
                    point.SortingCross();
                    foreach (var cross in point.Cross)
                    {
                        if (figure.CrossFiguresCompareExceptionFigure(cross.Position, figure.CrossFigures[cross.Figure]))
                        {
                            
                        }
                        else
                        {
                            result.Add(cross.Position);
                        }
                    }
                }
            }
            newFigures.Add(new Figure3(1001, points.ToArray()));
        }
        Debug.LogWarning($"ViewLinesController <color=cyan>MakePoints</color> result:{result.Count}");
        return result;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        foreach (var line in LinesView)
        {
            var a = Vector2To3(line.A);
            var b = Vector2To3(line.B);
            Handles.color = line.Color;
            Handles.DrawLine(a, b);
            if (line.Info != null)
                Handles.Label(Vector3.Lerp(a, b, .5f), $"{line.Index}_{line.Info.Face}");
        }
        foreach (var point in PointView)
        {
            var position = Vector2To3(point.Point);
            Gizmos.DrawSphere(position, 0.01f);
            Handles.Label(position + Vector3.up / 10f, $"{point.Index}");
        }
    }

    private Vector3 Vector2To3(Vector2 value)
    {
        return new Vector3(value.x, 0f, value.y);
    }

    private List<Figure3> MakeLines(List<WallData> walls)
    {
        var figures = new List<Figure3>();
        figures.Clear();
        for (int i = 0; i < walls.Count; i++)
        {
            var wall = walls[i];
            var a = wall.From;
            var b = wall.To;

            var magnitude = (a - b).magnitude;
            var p = WallLengthOffset / magnitude;
            var d = b - a;
            b += d * p;
            a -= d * p;

            var lines = new List<LinePoint>();
            var length = wall.Thickness / magnitude;
            var pointA = new Vector2(d.y, -d.x) * length / 2 + a;
            var pointB = a - (pointA - a);
            var pointC = pointB - a + b;
            var pointD = pointA - a + b;

            // Face 0 — ребро со стороны from
            lines.Add(new LinePoint(new WallInformationImpl(wall, 0), pointA));
            // Face 1 — ребро from -> to по часовой стрелке
            lines.Add(new LinePoint(new WallInformationImpl(wall, 1), pointB));
            // Face 2 — ребро to 
            lines.Add(new LinePoint(new WallInformationImpl(wall, 2), pointC));
            // Face 3 — ребро to -> from по часовой стрелке
            lines.Add(new LinePoint(new WallInformationImpl(wall, 3), pointD));

            // Назначаем данные о принадлежности стены комнатам
            // wall.Faces.Get().SetFaces(lines);
            var figure = new Figure3(i, lines.ToArray());
            figures.Add(figure);
        }

        return figures;
    }
}

internal class WallInformationImpl : IWallInformation
{
    public int Face { get; }

    public WallInformationImpl(WallData wall, int face)
    {
        Face = face;
    }
}
/*
public struct FigureRay
{
    public List<CrossPoint> CrossPoints;
    public List<CrossPoint> FigurePoints;

    public FigureRay(Vector2 from, Vector2 to, Figure3 figure)
    {
        CrossPoints = new List<CrossPoint>();
        FigurePoints = new List<CrossPoint>();
        for (int i = 0; i < figure.Points.Length; i++)
        {
            var point = figure.Points[i].Position;
            var normalize = (to - from).normalized;
            var lp = point - from;
            var result = from + normalize * Vector2.Dot(normalize, lp);
            FigurePoints.Add(new CrossPoint(result, i, normalize));
        }
        for (int i = 0; i < figure.LineCount; i++)
        {
            var line = figure.GetLine(i);
            if (FigureLine.CrossLine(line.A, line.B, from, to, out Vector2 result))
            {
                CrossPoints.Add(new CrossPoint(result, i, Vector2.zero));
            }
        }
        Debug.LogWarning($"FigureRay <color=cyan>FigureRay</color> CrossPoints:{CrossPoints.Count}");
    }
}

public struct CrossPoint
{
    public Vector2 Position { get; }
    public int Point { get; }
    public Vector2 Normalize { get; }

    public CrossPoint(Vector2 position, int point, Vector2 normalize)
    {
        Position = position;
        Point = point;
        Normalize = normalize;
    }
}
*/

public struct PointView
{
    public int Index;
    public Vector2 Point;

    public PointView(int index, Vector2 point)
    {
        Index = index;
        Point = point;
    }
}

public class LineView
{
    public Vector2 A { get; }
    public Vector2 B { get; }
    public IWallInformation Info { get; }
    public int Index;
    public Color Color;

    public LineView(int index, Vector2 a, Vector2 b, Color color, IWallInformation info = null)
    {
        Index = index;
        Color = color;
        A = a;
        B = b;
        Info = info;
    }
}

[System.Serializable]
public struct WallData : IWall
{
    public Vector2 From;
    public Vector2 To;
    public float Thickness;
}

public struct Figure3
{
    public int Index;

    public LinePoint[] Points;
    public List<Figure3> CrossFigures;
    // public Dictionary<Figure3, List<FigureCrossData>> FigureCrossData;

    public Figure3(int index, LinePoint[] points)
    {
        Index = index;
        Points = points;
        CrossFigures = new List<Figure3>();
        // FigureCrossData = new Dictionary<Figure3, List<FigureCrossData>>();
    }

    public int LineCount => Points.Length;

    public FigureLine GetLine(int point)
    {
        var pointA = Points[point];
        var pointB = point + 1 < Points.Length ? Points[point + 1] : Points[0];
        return new FigureLine(Index, pointA.Information, pointA.Position, pointB.Position);
    }

    public void AddCross(int lineA, Figure3 figureB, int lineB, Vector2 crossPoint)
    {
        CrossFigures.Add(figureB);
        Points[lineA].Cross.Add(new Cross(crossPoint, CrossFigures.Count - 1, lineB));
    }

    public bool Compare(Vector2 position)
    {
        var count = 0;
        for (int i = 0; i < LineCount; i++)
        {
            var line = GetLine(i);
            if (FigureLine.CrossLine(line.A, line.B, position, position + ViewLinesController.CompareRayLength, out Vector2 result))
            {
                count++;
            }
        }
        return count % 2 == 1;
    }
    public bool CrossFiguresCompare(Vector2 position)
    {
        var result = false;
        for (int k = 0; k < CrossFigures.Count; k++)
        {
            if (CrossFigures[k].Compare(position))
            {
                result = true;
                break;
            }
        }
        return result;
    }
    
    public bool CrossFiguresCompareExceptionFigure(Vector2 position, Figure3 figure)
    {
        var result = false;
        for (int k = 0; k < CrossFigures.Count; k++)
        {
            if (figure.Equals(CrossFigures[k]))
            {
                continue;
            }
            if (CrossFigures[k].Compare(position))
            {
                result = true;
                break;
            }
        }
        return result;
    }
}

public struct LinePoint
{
    public Vector2 Position;
    public IWallInformation Information;
    public List<Cross> Cross;

    public LinePoint(IWallInformation information, Vector2 position)
    {
        Information = information;
        Position = position;
        Cross = new List<Cross>();
    }

    public void SortingCross()
    {
        var position = Position;
        Cross.Sort((a, b) =>
        {
            if ((a.Position - position).magnitude > (b.Position - position).magnitude)
                return 1;
            return -1;
        });
    }
}

public struct Cross
{
    public Vector2 Position;
    public int Figure;
    public int Line;

    public Cross(Vector2 position, int figure, int line)
    {
        Position = position;
        Figure = figure;
        Line = line;
    }
}

public interface IWallInformation
{
    int Face { get; }
}