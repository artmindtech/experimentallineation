﻿using System.Collections.Generic;
using Models.Rooms.MedianPoint;
using UnityEditor;
using UnityEngine;

namespace Models.Rooms
{
    public class TestMedianPoints : MonoBehaviour
    {
        public static Vector2 DrawTarget = Vector2.negativeInfinity;
        [SerializeField] private bool Check;
        [SerializeField] private TextAsset AssetFile;
        [SerializeField] private bool AutoFillWithFile;
        [SerializeField] private Vector2[] TestPoints;
        [Range(0f, 1f)] [SerializeField] private float PathPosition = 0.4f;
        public MedianSettings MedianSettings = new MedianSettings();
        private List<LineRect> _testLines = new List<LineRect>();
        private MedianData _median;

        private void OnValidate()
        {
            if (AutoFillWithFile)
            {
                AutoFillWithFile = false;
                var json = AssetFile.text;
                if (!string.IsNullOrEmpty(json))
                {
                    var lines2 = JsonUtility.FromJson<TestLiniation2>(AssetFile.text);
                    TestPoints = new Vector2[lines2.Lines.Count];
                    for (int i = 0; i < lines2.Lines.Count; i++)
                    {
                        TestPoints[i] = lines2.Lines[i].B;
                    }
                }
            }

            if (!Check)
                return;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            var rectLines = new List<LineRect>();
            if (TestPoints != null && TestPoints.Length > 0)
            {
                _testLines.Clear();
                for (var i = 0; i < TestPoints.Length; i++)
                {
                    var b = TestPoints[i];
                    var a = i + 1 < TestPoints.Length ? TestPoints[i + 1] : TestPoints[0];
                    var lineRect = new LineRect( a.x, a.y, b.x, b.y);
                    rectLines.Add(lineRect);
                }
            }

            var builder = new SimpleMedianPointBuilder();
            _median = builder.BuildMedianPoints(rectLines, MedianSettings);
            _testLines = rectLines;
        }

        private void OnDrawGizmos()
        {
            if (!Check)
                return;
            Gizmos.color = Color.cyan;

            if (_testLines != null && _testLines.Count > 0)
            {
                for (int i = 0; i < _testLines.Count; i++)
                {
                    Vector3 a = new Vector3(_testLines[i].A.x, 0f, _testLines[i].A.y);
                    Vector3 b = new Vector3(_testLines[i].B.x, 0f, _testLines[i].B.y);
                    Gizmos.DrawLine(a, b);
                    // Handles.Label(a + Vector3.one / 10f, $"[{i}] a: {_testLines[i].A} {i} b: {_testLines[i].B} {_testLines[i].Angle * Mathf.Rad2Deg}");
                }
            }
            
            if (_median != null)
            {
                if (_median.DrawAngleDiagonal != null && _median.DrawAngleDiagonal.Count > 0)
                {
                    for (int i = 0; i < _median.DrawAngleDiagonal.Count; i++)
                    {
                        var line = _median.DrawAngleDiagonal[i].Line;
                        var curved = _median.DrawAngleDiagonal[i].Type == 0;
                        var test = _median.DrawAngleDiagonal[i].Type == 2;
                        Gizmos.color = curved ? Color.yellow : test ? Color.blue : Color.green;
                        Gizmos.color = Gizmos.color * 0.8f;
                        Vector3 a = new Vector3(line.Ax, 0f, line.Ay);
                        Vector3 b = new Vector3(line.Bx, 0f, line.By);
                        Gizmos.DrawLine(a, b);
                        if (curved)
                        {
                            Handles.Label(a + Vector3.one / 10f, $"{i}");
                        }
                        // Debug.Log(a + Vector3.one / 10f);
                    }
                }
                
                Gizmos.color = Color.red;
                var points = _median.GetPoints();
                for (int i = 0; i < points.Length; i++)
                {
                    Gizmos.DrawSphere(new Vector3(points[i].x, 0f, points[i].y), 0.01f);
                }

                var skeleton = _median.GetSkeleton();
                if (skeleton == null)
                    return;
                for (int i = 0; i < skeleton.Count; i++)
                {
                    var lineEdge = skeleton[i];
                    var a = new Vector3(lineEdge.A.x, 0f, lineEdge.A.y);
                    var b = new Vector3(lineEdge.B.x, 0f, lineEdge.B.y);
                    Handles.Label(b + Vector3.one / 50f, $"");
                    // Handles.Label(b - Vector3.one / 5f, $"{i}B");
                    Handles.Label(Vector3.Lerp(a, b, 0.5f), $"id:[{lineEdge.Ida}-{lineEdge.Idb}]");
                    Gizmos.DrawLine(a, b);
                }

                Gizmos.color = Color.cyan;

                var path = _median.GetPaths();
                var pathPosition = path[0].GetPathPosition(PathPosition);

                // Gizmos.DrawSphere(To3(pathPosition), 0.035f);
            }

            // Debug.LogWarning($"TestMedianPoints <color=blue>OnDrawGizmos</color> {DrawTarget} ");
            Gizmos.color = Color.cyan;
            if (DrawTarget != Vector2.negativeInfinity)
            {
                Gizmos.DrawSphere(new Vector3(DrawTarget.x, 0f, DrawTarget.y), 0.25f);
            }
        }

        private Vector3 To3(Vector2 pathPosition)
        {
            return new Vector3(pathPosition.x, 0f, pathPosition.y);
        }
    }
}