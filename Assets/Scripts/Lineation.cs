﻿using System.Collections.Generic;
using UnityEngine;

namespace Models.Rooms
{
    public class Lineation
    {
        private const int MaxCountLinieationSearching = 1400;
        private const float MinWallFace = 0.1f;
        public List<Lineation> Childs = new List<Lineation>();
        private LineationLine First => Lines[0];
        public List<LineationLine> Lines = new List<LineationLine>();
        private Vector2 _min;
        private Vector2 _max;

        public Lineation(LineationLine line)
        {
            Add(line);
        }

        private void Add(LineationLine line)
        {
            Lines.Add(line);
        }

        private LineationLine GetLastLine()
        {
            return Lines[Lines.Count - 1];
        }

        public bool InComplete()
        {
            return Lines.Count > 1 && GetLastLine().Line.CompereA(First.B);
        }

        public float GetSize()
        {
            var size = _max - _min;
            return size.x * size.y;
        }

        public void Searching(ref List<LineationLine> allLines, int c)
        {
            var start = Lines[0];
            allLines.Remove(start);
            var steps = new Dictionary<LineationLine, bool>();
            steps[start] = true;
            var next = start.GetPrevious();
            CalculateBBAA(next);
            var s = 0;
            while (next != start && s < MaxCountLinieationSearching)
            {
                s++;
                bool val;
                if (steps.TryGetValue(next, out val))
                {
                    for (int i = 0; i < Lines.Count; i++)
                    {
                        if (Lines[i] != next)
                        {
                            Lines.RemoveAt(i);
                            i--;
                        }
                    }

                    return;
                }

                Add(next);
                steps[next] = true;
                allLines.Remove(next);
                next = next.GetPrevious();
                CalculateBBAA(next);
            }
        }

        private void ClearBBAA()
        {
            _min = new Vector2(float.MaxValue, float.MaxValue);
            _max = new Vector2(float.MinValue, float.MinValue);
        }

        private void CalculateBBAA(LineationLine line)
        {
            _min.x = Mathf.Min(_min.x, line.A.x, line.B.x);
            _min.y = Mathf.Min(_min.y, line.A.y, line.B.y);
            _max.x = Mathf.Max(_max.x, line.A.x, line.B.x);
            _max.y = Mathf.Max(_max.y, line.A.y, line.B.y);
        }

        public void Approximation()
        {
            ClearBBAA();
            for (int i = 0; i < Lines.Count; i++)
            {
                var current = Lines[i];
                if (current.Length < MinWallFace)
                {
                    var parent = i - 1 > 0 ? Lines[i - 1] : Lines[Lines.Count - 1];
                    var next = i + 1 < Lines.Count ? Lines[i + 1] : Lines[0];
                    parent.SetA(next.B);
                    Lines.RemoveAt(i);
                    i--;
                    continue;
                }

                CalculateBBAA(current);
            }
        }

        

        public bool TrySetChild(Lineation other)
        {
            if (Compare(other))
            {
                Childs.Add(other);
                return true;
            }

            return false;
        }
        private bool Compare(Lineation other)
        {
            return other._max.x < _max.x && other._max.y < _max.y && other._min.x > _min.x && other._min.y > _min.y;
        }

        public Vector2[] GetVector2()
        {
            var c = Lines.Count;
            var result = new Vector2[c];
            for (int i = 0; i < c; i++)
            {
                result[i] = new Vector2(Lines[i].B.x, Lines[i].B.y);
            }
            return result;
        }

        public bool HasChildren()
        {
            return Childs.Count > 0;
        }
    }
}