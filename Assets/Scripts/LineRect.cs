﻿using UnityEngine;

namespace Models.Rooms
{
    [System.Serializable]
    public struct LineRect
    {
        public float Ax;
        public float Ay;
        public float Bx;
        public float By;
        public float MinX;
        public float MinY;
        public float MaxX;
        public float MaxY;
        public float Angle;

        public LineRect(float ax, float ay, float bx, float by)
        {
            Ax = ax;
            Ay = ay;
            Bx = bx;
            By = by;
            MinX = 0f;
            MaxX = 0f;
            MinY = 0f;
            MaxY = 0f;
            Angle = 0f;
            Calculate();
        }

        private void Calculate()
        {
            MinX = Mathf.Min(Ax, Bx);
            MaxX = Mathf.Max(Ax, Bx);
            MinY = Mathf.Min(Ay, By);
            MaxY = Mathf.Max(Ay, By);
            if (MaxX - MinX < 0.001f)
            {
                MinX -= 0.0005f;
                MaxX += 0.0005f;
            }

            if (MaxY - MinY < 0.001f)
            {
                MinY -= 0.0005f;
                MaxY += 0.0005f;
            }

            Angle = Mathf.Atan2(Ay - By, Ax - Bx);
        }

        public Vector2 B => new Vector2(Bx, By);
        public Vector2 A => new Vector2(Ax, Ay);
        public float Length => (B - A).magnitude;

        public bool Compere(LineRect rect)
        {
            return MaxX > rect.MinX && MinX < rect.MaxX && MaxY > rect.MinY && MinY < rect.MaxY;
        }

        public static bool CrossLine(LineRect a, LineRect b, ref Vector2 result)
        {
            var p1 = new Vector2(a.Ax, a.Ay);
            var p2 = new Vector2(a.Bx, a.By);
            var p3 = new Vector2(b.Ax, b.Ay);
            var p4 = new Vector2(b.Bx, b.By);
            if (AreCrossing(p1, p2, p3, p4))
            {
                float a1, b1, c1, a2, b2, c2;
                a1 = p2.y - p1.y;
                b1 = p1.x - p2.x;
                c1 = -p1.x * (p2.y - p1.y) + p1.y * (p2.x - p1.x);
                a2 = p4.y - p3.y;
                b2 = p3.x - p4.x;
                c2 = -p3.x * (p4.y - p3.y) + p3.y * (p4.x - p3.x);
                result = CrossingPoint(a1, b1, c1, a2, b2, c2);
                return true;
            }

            return false;
        }

        private static float VectorMult(float ax, float ay, float bx, float by) //векторное произведение
        {
            return ax * @by - bx * ay;
        }

        private static bool AreCrossing(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) //проверка пересечения
        {
            float v1 = VectorMult(p4.x - p3.x, p4.y - p3.y, p1.x - p3.x, p1.y - p3.y);
            float v2 = VectorMult(p4.x - p3.x, p4.y - p3.y, p2.x - p3.x, p2.y - p3.y);
            float v3 = VectorMult(p2.x - p1.x, p2.y - p1.y, p3.x - p1.x, p3.y - p1.y);
            float v4 = VectorMult(p2.x - p1.x, p2.y - p1.y, p4.x - p1.x, p4.y - p1.y);
            if (v1 * v2 < 0f && v3 * v4 < 0f)
                return true;
            return false;
        }

        private static Vector2 CrossingPoint(float a1, float b1, float c1, float a2, float b2, float c2)
        {
            Vector2 pt = new Vector2();
            float d = a1 * b2 - b1 * a2;
            float dx = -c1 * b2 + b1 * c2;
            float dy = -a1 * c2 + c1 * a2;
            pt.x = dx / d;
            pt.y = dy / d;
            return pt;
        }

        public void SetA(Vector2 value)
        {
            Ax = value.x;
            Ay = value.y;
            Calculate();
        }

        public void SetB(Vector2 value)
        {
            Bx = value.x;
            By = value.y;
            Calculate();
        }

        public override string ToString()
        {
            return base.ToString() + $"[{Ax}x{Ay}|{Bx}x{By}]";
        }
    }
}