﻿using System.Collections.Generic;
using UnityEngine;

namespace Models.Rooms
{
    public class LineationLine
    {
        public WallLine Line;
        private List<LineationLine> _previous = new List<LineationLine>();
        private List<LineationLine> _next = new List<LineationLine>();

        public LineationLine(WallLine line)
        {
            Line = line;
        }

        public string Why { get; set; }
        public Vector2 B => Line.B;
        public Vector2 A => Line.A;
        public float Length => (B - A).magnitude;

        public void AddPrevious(LineationLine line)
        {
            _previous.Add(line);
            line._next.Add(this);
        }

        public bool HasPrevious()
        {
            return _previous.Count > 0;
        }

        public bool HasNext()
        {
            return _next.Count > 0;
        }

        public LineationLine GetPrevious()
        {
            return _previous[0];
        }

        public void RemoveNext(List<LineationLine> removes)
        {
            foreach (var child in _next)
            {
                child._previous.Remove(this);
                if (!child.HasPrevious())
                {
                    child.Why = $" Next No Previous [{this}]";
                    removes.Add(child);
                }
            }

            _next.Clear();
        }

        public void ClearPrevious()
        {
            _previous.Clear();
        }

        public override string ToString()
        {
            return "LL:" + Line;
        }

        // public bool IsSeriousLine()
        // {
        //     return !Line.IsButtEnd() && Line.Length > RoomsDetector.MinWallFace;
        // }

        public void SetA(Vector2 a)
        {
            Line.SetA(a);
        }
        
        public void SetB(Vector2 b)
        {
            Line.SetB(b);
        }
    }
}