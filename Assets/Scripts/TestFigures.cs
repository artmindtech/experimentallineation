﻿using System;
using System.Collections.Generic;
using Models.Rooms;
using UnityEditor;
using UnityEngine;

public class TestFigures : MonoBehaviour
{
    public bool AutoReload = true;
    public bool ShowWalls = true;
    public bool ShowFigure;
    public bool SubtractFigure = true;
    [Range(0, 10)] public int ShowFigures = 10;
    [Range(0, 10)] public int HideFigures = 10;
    public TestWall[] Walls;
    private List<Figure> _figures = new List<Figure>();
    private static List<WallLine> _show;

    private void OnValidate()
    {
        if(!AutoReload)
            return;
        _figures.Clear();
        if (Walls != null && Walls.Length >= 2)
        {
            foreach (var testWall in Walls)
            {
                _figures.Add(new Figure(testWall.GetWalls()));
            }

            if (SubtractFigure)
            {
                var targetSubtract = _figures[0];
                for (int i = 1; i < _figures.Count; i++)
                {
                    targetSubtract.Subtraction(_figures[i]);
                }
            }
            else
            {
                var targetSumm = _figures[0];
                for (int i = 1; i < _figures.Count; i++)
                {
                    targetSumm.Join(_figures[i]);
                    _figures.RemoveAt(i);
                    i--;
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (_show != null)
        {
            Gizmos.color = Color.cyan;

            for (int i = 0; i < _show.Count; i++)
            {
                var wallLine = _show[i];
                Gizmos.DrawLine(new Vector3(wallLine.A.x, 0.02f, wallLine.A.y),
                    new Vector3(wallLine.B.x, 0.02f, wallLine.B.y));
            }
        }

        if (ShowWalls && Walls != null)
        {
            for (int i = 0; i < Walls.Length; i++)
            {
                var wall = Walls[i];
                var lines = wall.GetWallLines();
                Gizmos.color = Color.cyan * new Color(1, 1, 1, 0.9f);
                DrawWallLine(wall);
                Gizmos.color = Color.red * new Color(1, 1, 1, 0.9f);
                foreach (var wl in lines)
                {
                    var a = wl.A;
                    var b = wl.B;
                    Gizmos.DrawLine(new Vector3(a.x, -0.001f, a.y), new Vector3(b.x, -0.001f, b.y));
                }
            }
        }

        if (ShowFigure && _figures.Count > 0)
        {
            int i = 0;
            if (SubtractFigure)
            {
                foreach (var figure in _figures)
                {
                    if (i++ < ShowFigures)
                    {
                        if (i < HideFigures)
                        {
                            continue;
                        }

                        for (int j = 0; j < figure.Lines.Count; j++)
                        {
                            Gizmos.color = figure.Lines[j].NeedWall ? Color.green : Color.yellow;
                            Gizmos.color *= new Color(1f, 1f, 1f, 0.9f);
                            DrawFigureLine(figure, j);
                        }
                    }
                }
            }
            else
            {
                Gizmos.color = Color.white;

                foreach (var figure in _figures)
                {
                    if (i++ < ShowFigures)
                    {
                        for (int j = 0; j < figure.Lines.Count; j++)
                        {
                            DrawFigureLine(figure, j);
                        }
                    }
                }
            }
        }
    }

    private void DrawFigureLine(Figure figure, int index, float y = 0.01f)
    {
        var figureLine = figure.Lines[index];
        var start = new Vector3(figureLine.A.x, y, figureLine.A.y);
        Gizmos.DrawLine(start,
            new Vector3(figureLine.B.x, y, figureLine.B.y));
        Handles.Label(start + (Vector3.up / 15f), $"f:{figure.GetIndex()}_{index}");
    }

    private void DrawWallLine(TestWall wall, float y = 0f)
    {
        Gizmos.DrawLine(new Vector3(wall.From.x, y, wall.From.y), new Vector3(wall.To.x, y, wall.To.y));
    }

    public override string ToString()
    {
        return base.ToString() + "_figures count:" + _figures.Count;
    }

    public static void Show(List<WallLine> newLines)
    {
        _show = newLines;
    }

    public void SetFigures()
    {
        throw new NotImplementedException();
    }

    public void SetFigures(Figure fa, Figure fb)
    {
        AutoReload = false;
        _figures.Clear();
        _figures.Add(fa);
        _figures.Add(fb);
        // Debug.LogWarning($"TestFigures <color=blue>SetFigures</color> fa:{fa.Lines.Count}");
        // Debug.LogWarning($"TestFigures <color=blue>SetFigures</color> fb:{fb.Lines.Count}");
    }
}