﻿using System;
using System.Collections.Generic;

[Serializable]
public class TestData
{
    public List<WallLine> FigureA;
    public List<WallLine> FigureB;
}