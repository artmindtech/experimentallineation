﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Models.Rooms;
using UnityEditor;
using UnityEngine;

public class TestDataView : MonoBehaviour
{
    private static List<Vector3> DrawPolygon = new List<Vector3>();
    public bool Animation;
    public TextAsset TextAsset;
    public bool ReadFigures;
    public TestFigures TestFigures;
    public bool ReadHarutPolygon;
    private TestData _testData;

    private void OnValidate()
    {
        if (TextAsset == null)
        {
            Debug.LogWarning($"TestDataView <color=blue>Figures</color> No Json Data!");
            return;
        }

        if (ReadFigures)
        {
            Figures();
        }

        if (ReadHarutPolygon)
        {
            HarutPolygon();
        }
    }

    private void HarutPolygon()
    {
        string[] points = Regex.Split(TextAsset.text, @"\]\,\ \[");
        if (Animation)
        {
            return;
        }

        DrawPolygon.Clear();
        for (int i = 0; i < points.Length; i++)
        {
            string[] values = Regex.Split(points[i], @"\,\ ");
            var a = ValueParse(values[0]) / 100f;
            var b = ValueParse(values[1]) / 100f;
            DrawPolygon.Add(new Vector3(a, 0, b));
        }

        Debug.LogWarning($"TestDataView <color=blue>HarutPolygon</color> Polygon Count:{points.Length}");
    }

    private float ValueParse(string value)
    {
        return float.Parse(value
            .Replace(".", ",")
            .Replace("[[", "")
            .Replace("]]", ""));
    }

    private void Figures()
    {
        _testData = JsonUtility.FromJson<TestData>(TextAsset.text);
        if (_testData == null)
        {
            Debug.LogWarning($"TestDataView <color=blue>Figures</color> Data Not Valid");
            return;
        }

        var wl = new List<WallLine>();
        foreach (var wallLine in _testData.FigureA)
        {
            wallLine.CalculateRect();
            wl.Add(wallLine);
        }

        var fa = new Figure(wl);

        wl = new List<WallLine>();
        foreach (var wallLine in _testData.FigureB)
        {
            wallLine.CalculateRect();
            wl.Add(wallLine);
        }

        var fb = new Figure(wl);

        fa.Subtraction(fb);

        TestFigures.SetFigures(fa, fb);

        var linesA = LineationDetector.DetectPolygonsByLines(fa.Lines);

        var linesB = LineationDetector.DetectPolygonsByLines(fb.Lines);

        Debug.LogWarning(
            $"TestDataView <color=blue>OnValidate</color> linesA:{linesA.Count} {linesB.Count} Childs:{linesB[0].Childs.Count}");
    }

    private void OnDrawGizmos()
    {
        Animation = true;
        for (int i = 0; i < DrawPolygon.Count - 1; i++)
        {
            Handles.DrawLine(DrawPolygon[i], DrawPolygon[i + 1]);
        }

        Animation = false;
    }
}