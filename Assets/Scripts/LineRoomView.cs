﻿using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(LineRenderer))]
public class LineRoomView : MonoBehaviour
{
    [SerializeField] private MeshWalls meshWalls;
    [SerializeField] private LineRenderer lineRenderer;

    private void OnValidate()
    {
        meshWalls = FindObjectOfType<MeshWalls>();
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame3
    void Update()
    {
        Vector3[] positions = new Vector3[meshWalls.WallPoints.Length];
        for (int i = 0; i < meshWalls.WallPoints.Length; i++)
        {
            positions[i] = new Vector3(meshWalls.WallPoints[i].Position.x, 0, meshWalls.WallPoints[i].Position.y);
        }

        // Debug.LogWarning($"LineRoomView <color=blue>Update</color> positions:{positions.Length}");
        lineRenderer.positionCount = positions.Length + 1;
        lineRenderer.SetPositions(positions);
        lineRenderer.SetPosition(positions.Length, positions[0]);
        lineRenderer.useWorldSpace = true;
    }
}