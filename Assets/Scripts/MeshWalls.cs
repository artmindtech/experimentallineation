﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class MeshWalls : MonoBehaviour
{
    public WallPoint[] WallPoints;
    public Material SecondaryMaterial;
    [SerializeField] private MeshFilter meshFilter;
    [SerializeField] private MeshRenderer meshRenderer;
    private Mesh _mesh;
    private Vector3[] _meshVertices;
    private Vector2[] _meshUv;
    private int[][] _meshTrianglesMain;
    private int[] _meshTrianglesSecondary;
    private int _vertexesCount;
    private int _secondaryTrianglesCount;
    private float _uLength;

    // Start is called before the first frame update
    public void OnValidate()
    {
        meshFilter = GetComponent<MeshFilter>();
        if (_mesh == null)
        {
            _mesh = new Mesh();
            _mesh.name = "GenerateWall";
        }
        else
        {
            _mesh.Clear();
        }

        meshRenderer = meshRenderer ?? GetComponent<MeshRenderer>();
        DrawMesh();
    }

    private void Awake()
    {
        _mesh = new Mesh();
        _mesh.name = "GenerateWall";
        meshFilter.mesh = _mesh;
        DrawMesh();
    }

    private void DrawMesh()
    {
        var count = WallPoints.Length;

        _vertexesCount = 0;
        _uLength = 0;
        _meshTrianglesMain = new int[count][];

        var meshesData = new MeshData[WallPoints.Length];
        var allMaterials = new Material[count + 1];
        var countVertexes = 0;
        var countSecondaryTriangles = 0;
        for (int i = 0; i < count; i++)
        {
            var nextPoint = WallPoints[0];
            if (i + 1 < count)
                nextPoint = WallPoints[i + 1];

            MeshData data = new MeshData();
            var height = 3f;

            var secondaryHeight = i == 0 ? 1.6f : height;
            data.Init(WallPoints[i], nextPoint.Position, secondaryHeight, height);
            countVertexes += data.GetVertexCount();
            _meshTrianglesMain[i] = new int[data.GetMainTrianglesCount()];
            countSecondaryTriangles += data.GetSecondaryTrianglesCount();
            meshesData[i] = data;
            allMaterials[i] = WallPoints[i].Material;
        }

        allMaterials[count] = SecondaryMaterial;

        _meshVertices = new Vector3[countVertexes];
        _meshUv = new Vector2[countVertexes];
        _meshTrianglesSecondary = new int[countSecondaryTriangles];
        _secondaryTrianglesCount = 0;

        for (int i = 0; i < count; i++)
            Generate(meshesData, i);

        _mesh.vertices = _meshVertices;
        _mesh.uv = _meshUv;
        _mesh.subMeshCount = count + 1;
        for (int i = 0; i < count; i++)
            _mesh.SetTriangles(_meshTrianglesMain[i], i);
        _mesh.SetTriangles(_meshTrianglesSecondary, count);
        _mesh.RecalculateNormals();
        _mesh.RecalculateTangents();
        meshFilter.mesh = _mesh;
        meshRenderer.materials = allMaterials;
    }

    private void Generate(MeshData[] dates, int index)
    {
        var date = dates[index];
        MeshPoint[][] points = date.VertexPoints;
        int ySize = points.Length - 1;
        int xSize = points[0].Length - 1;
        float length = date.Length;
        int trianglesCount = 0;
        var holesCount = 0;

        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                var meshPoint = points[y][x];
                _meshVertices[_vertexesCount + i] = meshPoint.Position;
                _meshUv[_vertexesCount + i] = new Vector2(_uLength + meshPoint.Uv.x, meshPoint.Uv.y);
            }
        }

        int ti = 0, vi = 0;
        for (int y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                var meshPoint = points[y][x];
                if (meshPoint.Hole)
                {
                    holesCount += 6;
                    continue;
                }

                if (meshPoint.Secondary)
                {
                    _meshTrianglesSecondary[_secondaryTrianglesCount] = _vertexesCount + vi;
                    _meshTrianglesSecondary[_secondaryTrianglesCount + 1] =
                        _meshTrianglesSecondary[_secondaryTrianglesCount + 4] = _vertexesCount + vi + xSize + 1;
                    _meshTrianglesSecondary[_secondaryTrianglesCount + 2] =
                        _meshTrianglesSecondary[_secondaryTrianglesCount + 3] = _vertexesCount + vi + 1;
                    _meshTrianglesSecondary[_secondaryTrianglesCount + 5] = _vertexesCount + vi + xSize + 2;
                    _secondaryTrianglesCount += 6;
                }
                else
                {
                    _meshTrianglesMain[index][trianglesCount] = _vertexesCount + vi;
                    _meshTrianglesMain[index][trianglesCount + 1] =
                        _meshTrianglesMain[index][trianglesCount + 4] = _vertexesCount + vi + xSize + 1;
                    _meshTrianglesMain[index][trianglesCount + 2] =
                        _meshTrianglesMain[index][trianglesCount + 3] = _vertexesCount + vi + 1;
                    _meshTrianglesMain[index][trianglesCount + 5] = _vertexesCount + vi + xSize + 2;
                    trianglesCount += 6;
                }
            }
        }

        _vertexesCount += (xSize + 1) * (ySize + 1);
        _uLength += xSize * length / xSize;
    }
}

public struct MeshPoint
{
    public Vector3 Position;
    public Vector2 Uv;
    public bool Hole;
    public bool Secondary;

    public MeshPoint(Vector3 position, Vector2 uv, bool hole, bool secondary)
    {
        Position = position;
        Uv = uv;
        Hole = hole;
        Secondary = secondary;
    }
}

public struct MeshData
{
    public MeshPoint[][] VertexPoints;
    public float Length;
    private GridSettings _grid;

    struct GridSettings
    {
        public int XSize => _xSlice.Count;
        public int YSize => _ySlice.Count;
        public int DeleteMainTriangles => _deleteMainTriangles;
        public int DeleteSecondaryTriangles => _deleteSecondaryTriangles;
        public int YSecondarySize => _ySecondarySize;

        private readonly List<float> _xSlice;
        private readonly List<float> _ySlice;
        private readonly List<bool> _ySecondary;
        private readonly float _length;
        private readonly float _height;
        private int _deleteMainTriangles;
        private int _deleteSecondaryTriangles;
        private int _ySecondarySize;
        private float _secondaryHeight;

        public GridSettings(float length, float height)
        {
            _length = length;
            _height = height;
            _xSlice = new List<float> {0f};
            _ySlice = new List<float> {0f};
            _ySecondary = new List<bool> {false};
            _deleteMainTriangles = 0;
            _deleteSecondaryTriangles = 0;
            _ySecondarySize = 0;
            _secondaryHeight = height + 0.1f;
        }

        public void SetSecondaryHeight(float secondaryHeight)
        {
            _secondaryHeight = secondaryHeight;
            AddY(_secondaryHeight);
        }

        public void AddX(float x)
        {
            _xSlice.Add(Mathf.Clamp(x, 0f, _length));
        }

        public void AddY(float y)
        {
            _ySlice.Add(Mathf.Clamp(y, 0f, _height));
            _ySecondary.Add(y >= _secondaryHeight);
        }

        /// <summary>
        /// Алгоритм расчета сетки стены с учетом отверстий
        /// </summary>
        public MeshPoint[][] Calculate(List<Rect> holes, Vector3 currentPosition, Vector3 nextPosition)
        {
            _xSlice.Sort();
            _ySlice.Sort();
            _ySecondary.Sort();

            // Добавляем конечные значения
            AddX(_length);
            AddY(_height);

            // Удаляем дублирующиеся срезы
            for (int y = 0; y < _ySlice.Count - 2; y++)
                if (Mathf.Abs(_ySlice[y + 1] - _ySlice[y]) < 0.001f)
                {
                    _ySecondary.RemoveAt(y + 1);
                    _ySlice.RemoveAt(y + 1);
                }

            for (int x = 0; x < _xSlice.Count - 2; x++)
                if (Mathf.Abs(_xSlice[x + 1] - _xSlice[x]) < 0.001f)
                    _xSlice.RemoveAt(x + 1);

            // Создаем данные сетки
            _deleteMainTriangles = 0;
            _deleteSecondaryTriangles = 0;
            _ySecondarySize = 0;
            var vertexPoints = new MeshPoint[_ySlice.Count][];
            for (int y = 0; y < _ySlice.Count; y++)
            {
                vertexPoints[y] = vertexPoints[y] ?? new MeshPoint[_xSlice.Count];
                var secondary = _ySecondary[y];
                if (secondary && y + 1 < _ySlice.Count)
                {
                    _ySecondarySize++;
                }

                for (int x = 0; x < _xSlice.Count; x++)
                {
                    var hole = false;
                    var px = _xSlice[x];
                    var py = _ySlice[y];

                    if (x < _xSlice.Count - 1 && y < _ySlice.Count - 1)
                    {
                        var testPoint = new Vector2(
                            Mathf.Lerp(_xSlice[x + 1], px, 0.5f),
                            Mathf.Lerp(_ySlice[y + 1], py, 0.5f));
                        for (int i = 0; i < holes.Count; i++)
                        {
                            hole = holes[i].Contains(testPoint);
                            if (hole)
                            {
                                if (secondary)
                                    _deleteSecondaryTriangles += 6;
                                else
                                    _deleteMainTriangles += 6;
                                break;
                            }
                        }
                    }

                    var xPos = Mathf.Lerp(currentPosition.x, nextPosition.x, px / _length);
                    var yPos = py;
                    var zPos = Mathf.Lerp(currentPosition.y, nextPosition.y, px / _length);
                    Vector2 uv = new Vector2(px, py);

                    vertexPoints[y][x] = new MeshPoint(new Vector3(xPos, yPos, zPos), uv, hole, secondary);
                }
            }

            return vertexPoints;
        }
    }

    public void Init(WallPoint point, Vector2 nextPosition, float secondaryHeight, float height)
    {
        var currentPosition = point.Position;
        Length = (currentPosition - nextPosition).magnitude;
        _grid = new GridSettings(Length, height);
        var gridHoles = new List<Rect>();
        var hasSecondary = secondaryHeight < height;
        if (hasSecondary)
        {
            _grid.SetSecondaryHeight(secondaryHeight);
        }

        if (point.Holes.Length != 0)
        {
            for (int i = 0; i < point.Holes.Length; i++)
            {
                var hole = point.Holes[i];
                var holPosition = hole.AttachOnWall(currentPosition, nextPosition);
                var angle = Mathf.Atan2(nextPosition.y - currentPosition.y, nextPosition.x - currentPosition.x);
                var holeHalfSizeX = hole.Size.x / 2f;
                var holeSizeY = hole.Size.y;
                var holDelta = new Vector2(Mathf.Cos(angle) * holeHalfSizeX, Mathf.Sin(angle) * holeHalfSizeX);

                // Проверяем принадлежность краев отверстия стене
                var lengthRight = (nextPosition - (holPosition + holDelta)).magnitude;
                var lengthLeft = (currentPosition - (holPosition - holDelta)).magnitude;
                if (lengthRight > Length || lengthLeft > Length)
                {
                    // Отверстие не пренадлежит стене
                }
                else
                {
                    // Вертикальные срезы
                    var rightSideOff = (nextPosition - holPosition).magnitude <= holDelta.magnitude;
                    var leftSideOff = (currentPosition - holPosition).magnitude <= holDelta.magnitude;
                    var holeRectPosition = new Vector2((holPosition - holDelta - currentPosition).magnitude, hole.Y);
                    var holeRectSize = new Vector2(hole.Size.x, holeSizeY);

                    if (rightSideOff && leftSideOff)
                    {
                        // Отверстие выходит за стену с двух сторон
                        holeRectPosition.x = 0f;
                    }
                    else if (rightSideOff)
                    {
                        // Отверстие выходит справа
                        _grid.AddX((currentPosition - (holPosition - holDelta)).magnitude);
                    }
                    else if (leftSideOff)
                    {
                        // Отверстие выходит слева
                        holeRectSize.x -= holeRectPosition.x;
                        holeRectPosition.x = 0f;
                        _grid.AddX((currentPosition - (holPosition + holDelta)).magnitude);
                    }
                    else
                    {
                        // Отверстие в центре
                        _grid.AddX((currentPosition - (holPosition - holDelta)).magnitude);
                        _grid.AddX((currentPosition - (holPosition + holDelta)).magnitude);
                    }

                    // Горизонтальные срезы
                    var upSideOff = height <= hole.Y + holeSizeY;
                    var downSideOff = hole.Y <= 0f;

                    if (upSideOff && downSideOff)
                    {
                        // Отверстие выходит за стену сверху и снизу
                    }
                    else if (upSideOff)
                    {
                        // Отверстие выходит вверх
                        _grid.AddY(hole.Y);
                    }
                    else if (downSideOff)
                    {
                        // Отверстие выходит вниз
                        _grid.AddY(hole.Y + holeSizeY);
                    }
                    else
                    {
                        // Отверстие в центре
                        _grid.AddY(hole.Y);
                        _grid.AddY(hole.Y + holeSizeY);
                    }

                    var holeRect = new Rect(holeRectPosition, holeRectSize);
                    gridHoles.Add(holeRect);
                }
            }
        }

        VertexPoints = _grid.Calculate(gridHoles, currentPosition, nextPosition);
    }

    public int GetVertexCount()
    {
        return _grid.XSize * _grid.YSize;
    }

    public int GetSecondaryTrianglesCount()
    {
        return (_grid.XSize - 1) * (_grid.YSecondarySize) * 6 - _grid.DeleteSecondaryTriangles;
    }

    public int GetMainTrianglesCount()
    {
        return (_grid.XSize - 1) * (_grid.YSize - 1) * 6 - _grid.DeleteMainTriangles - GetSecondaryTrianglesCount();
    }
}

[Serializable]
public class WallPoint
{
    public Vector2 Position;
    public Hole[] Holes;
    public Material Material;
}

[Serializable]
public class Hole
{
    public Vector2 Position;
    public Vector2 Size;
    public float Y;

    public Vector2 AttachOnWall(Vector2 currentPosition, Vector2 nextPosition)
    {
        var normalize = (currentPosition - nextPosition).normalized;
        var delta = Position - currentPosition;
        return currentPosition + normalize * Vector2.Dot(normalize, delta);
    }
}