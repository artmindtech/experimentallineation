﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TestWall
{
    public Vector2 From = new Vector2(0f, 0f);
    public Vector2 To = new Vector2(10f, 0f);
    public float Offset = 0.1f;
    public float Depth = 1f;
    private Vector3 _to;
    private Vector3 _from;

    // private Vector2 _rotatePos;
    private List<WallLine> _lines = new List<WallLine>();

    public List<WallLine> GetWalls()
    {
        var from = From;
        var to = To;
        CalculateWallLines(from, to, Depth);
        _from = new Vector3(from.x, 0f, from.y);
        _to = new Vector3(to.x, 0f, to.y);
        return _lines;
    }

    private void CalculateWallLines(Vector2 a, Vector2 b, float depth)
    {
        _lines.Clear();
        var magnitude = (a - b).magnitude;

        var p = Offset / magnitude;
        var wall = b - a;
        a = a - wall * p;
        b = b + wall * p;
        var length = depth / magnitude;
        var pointA = new Vector2(wall.y, -wall.x) * length / 2 + a;
        var pointB = a - (pointA - a);
        var pointC = pointB - a + b;
        var pointD = pointA - a + b;
        
        _lines.Add(new WallLine(null, 0, pointA, pointB));
        _lines.Add(new WallLine(null, 0, pointB, pointC));
        _lines.Add(new WallLine(null, 0, pointC, pointD));
        _lines.Add(new WallLine(null, 0, pointD, pointA));
        
        if (depth > 2f)
        {
            var center = Vector2.Lerp(pointA, pointC, 0.5f);
            _lines.Add(new WallLine(null, 0, center, center + new Vector2(0.5f,0)));
            _lines.Add(new WallLine(null, 0, center + new Vector2(0.5f,0), center + new Vector2(0.5f,0.5f)));
            _lines.Add(new WallLine(null, 0, center + new Vector2(0.5f,0.5f), center + new Vector2(0f,0.5f)));
            _lines.Add(new WallLine(null, 0, center + new Vector2(0f,0.5f), center));
        }
        //
        // _lines.Add(new WallLine(null, 0, pointA, pointB));
        // _lines.Add(new WallLine(null, 0, pointB, pointC));
        // _lines.Add(new WallLine(null, 0, pointC, pointD));
        // _lines.Add(new WallLine(null, 0, pointD, pointA));
        //
    }

    public List<WallLine> GetWallLines()
    {
        return _lines;
    }
}