﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class MeshWallsOld : MonoBehaviour
{
    public Vector2[] WallPoints;
    [SerializeField] private MeshFilter meshFilter;
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private GameObject marker;
    private Mesh _mesh;
    private Vector3[] _meshVertices;
    private Vector2[] _meshUv;
    private Vector4[] _meshTangents;
    private int[] _meshTriangles;
    [SerializeField] private List<GameObject> _allMarkers = new List<GameObject>();

    // Start is called before the first frame update
    void OnValidate()
    {
        meshFilter = GetComponent<MeshFilter>();
        _mesh = new Mesh();
        _mesh.name = "Grid";
        meshRenderer = GetComponent<MeshRenderer>();
        DrawMesh(WallPoints);
        
        var from = new Vector2(0.0f, 0f);
        var to = new Vector2(1f, 0.2f);
        var point = new Vector2(0.5f, 0.7f);

        var normalize = (from - to).normalized;
        var lp = point - from;
        var result = normalize * Vector2.Dot(normalize, lp);
    }

    private void Awake()
    {
        _mesh = new Mesh();
        _mesh.name = "Grid";
        meshFilter.mesh = _mesh;
    }

    private void DrawMesh(Vector2[] points)
    {
        var count = points.Length;
        var xSize = 3;
        var ySize = 3;

        _meshVertices = new Vector3[(xSize + 1) * (ySize + 1) * count];
        _meshUv = new Vector2[_meshVertices.Length];
        _meshTangents = new Vector4[_meshVertices.Length];
        _meshTriangles = new int[xSize * ySize * 6 * count];

        for (int i = 0; i < count; i++)
        {
            var point = points[i];
            var nextPoint = points[count - 1];
            if (i - 1 != -1)
                nextPoint = points[i - 1];
            
            Vector3[][] vertexPoints = new Vector3[ySize + 1][];
            var wallLength = (point - nextPoint).magnitude;
            for (int y = 0; y <= ySize; y++)
            {
                vertexPoints[y] = new Vector3[xSize + 1];
                for (int x = 0; x <= xSize; x++)
                {
                    var xPos = Mathf.Lerp(point.x, nextPoint.x, x / (float) xSize);
                    var yPos = y;
                    var zPos = Mathf.Lerp(point.y, nextPoint.y, x / (float) ySize);
                    vertexPoints[y][x] = new Vector3(xPos, yPos, zPos);
                }
            }

            var angle = Mathf.Atan2(nextPoint.y - point.y, nextPoint.x - point.x);

            Generate(vertexPoints, angle, i, xSize, ySize, wallLength);
        }

        _mesh.vertices = _meshVertices;
        _mesh.uv = _meshUv;
        _mesh.triangles = _meshTriangles;
        _mesh.RecalculateNormals();
        // _mesh.tangents = _meshTangents;
        meshFilter.mesh = _mesh;
    }

    private void Generate(Vector3[][] points, float angle, int index, int xSize, int ySize, float wallLength)
    {
        // float tangentX = Mathf.Cos(angle);
        // float tangentY = Mathf.Sin(angle);
        // var tangent = new Vector4(tangentX, tangentY, 0f, 1f);
        
        var tangent = new Vector4(1f, 0f, 0f, 1f);
        var startVertexes = index * (xSize + 1) * (ySize + 1);
        var startUv = startVertexes;
        var startTangents = startVertexes;
        var startTriangles = index * xSize * ySize * 6;
        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                _meshVertices[startVertexes + i] = points[y][x];
                _meshUv[startUv + i] = new Vector2(x * wallLength / xSize, y);
                _meshTangents[startTangents + i] = tangent;
            }
        }

        int ti = 0, vi = 0;
        for (int y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                _meshTriangles[startTriangles + ti] = startVertexes + vi;
                _meshTriangles[startTriangles + ti + 1] =
                    _meshTriangles[startTriangles + ti + 4] = startVertexes + vi + xSize + 1;
                _meshTriangles[startTriangles + ti + 2] =
                    _meshTriangles[startTriangles + ti + 3] = startVertexes + vi + 1;
                _meshTriangles[startTriangles + ti + 5] = startVertexes + vi + xSize + 2;
            }
        }
    }
}