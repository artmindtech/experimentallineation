using System.Collections.Generic;
using UnityEngine;

public struct MeshData2
{
    public MeshPoint2[][] VertexPoints;
    public float Length;
    private GridSettings _grid;

    struct GridSettings
    {
        public int XSize => _xSlice.Count;
        public int YSize => _ySlice.Count;
        public int DeleteMainTriangles => _deleteMainTriangles;

        private readonly List<float> _xSlice;
        private readonly List<float> _ySlice;
        private readonly float _length;
        private readonly float _height;
        private int _deleteMainTriangles;
        private float _secondaryHeight;

        public GridSettings(float length, float height)
        {
            _length = length;
            _height = height;
            _xSlice = new List<float> {0f};
            _ySlice = new List<float> {0f};
            _deleteMainTriangles = 0;
            _secondaryHeight = height + 0.1f;
        }

        public void AddX(float x)
        {
            _xSlice.Add(Mathf.Clamp(x, 0f, _length));
        }

        public void AddY(float y)
        {
            _ySlice.Add(Mathf.Clamp(y, 0f, _height));
        }

        /// <summary>
        /// Алгоритм расчета сетки стены с учетом отверстий
        /// </summary>
        public MeshPoint2[][] Calculate(List<Rect> holes, Vector3 currentPosition, Vector3 nextPosition)
        {
            _xSlice.Sort();
            _ySlice.Sort();

            // Добавляем конечные значения
            AddX(_length);
            AddY(_height);

            // Удаляем дублирующиеся срезы
            for (int y = 0; y < _ySlice.Count - 2; y++)
                if (Mathf.Abs(_ySlice[y + 1] - _ySlice[y]) < 0.001f)
                {
                    _ySlice.RemoveAt(y + 1);
                }

            for (int x = 0; x < _xSlice.Count - 2; x++)
                if (Mathf.Abs(_xSlice[x + 1] - _xSlice[x]) < 0.001f)
                    _xSlice.RemoveAt(x + 1);

            // Создаем данные сетки
            _deleteMainTriangles = 0;
            var vertexPoints = new MeshPoint2[_ySlice.Count][];
            for (int y = 0; y < _ySlice.Count; y++)
            {
                vertexPoints[y] = vertexPoints[y] ?? new MeshPoint2[_xSlice.Count];

                for (int x = 0; x < _xSlice.Count; x++)
                {
                    var hole = false;
                    var px = _xSlice[x];
                    var py = _ySlice[y];

                    if (x < _xSlice.Count - 1 && y < _ySlice.Count - 1)
                    {
                        var testPoint = new Vector2(
                            Mathf.Lerp(_xSlice[x + 1], px, 0.5f),
                            Mathf.Lerp(_ySlice[y + 1], py, 0.5f));
                        for (int i = 0; i < holes.Count; i++)
                        {
                            hole = holes[i].Contains(testPoint);
                            if (hole)
                            {
                                _deleteMainTriangles += 6;
                                break;
                            }
                        }
                    }

                    var xPos = Mathf.Lerp(currentPosition.x, nextPosition.x, px / _length);
                    var yPos = py;
                    var zPos = Mathf.Lerp(currentPosition.y, nextPosition.y, px / _length);
                    Vector2 uv = new Vector2(px, py);

                    vertexPoints[y][x] = new MeshPoint2(new Vector3(xPos, yPos, zPos), uv, hole);
                }
            }

            return vertexPoints;
        }
    }

    public void Init(WallPoint point, Vector2 nextPosition, float height)
    {
        var currentPosition = point.Position;
        Length = (currentPosition - nextPosition).magnitude;
        _grid = new GridSettings(Length, height);
        var gridHoles = new List<Rect>();

        if (point.Holes.Length != 0)
        {
            for (int i = 0; i < point.Holes.Length; i++)
            {
                var hole = point.Holes[i];
                var holPosition = hole.AttachOnWall(currentPosition, nextPosition);
                var angle = Mathf.Atan2(nextPosition.y - currentPosition.y, nextPosition.x - currentPosition.x);
                var holeHalfSizeX = hole.Size.x / 2f;
                var holeSizeY = hole.Size.y;
                var holDelta = new Vector2(Mathf.Cos(angle) * holeHalfSizeX, Mathf.Sin(angle) * holeHalfSizeX);

                // Проверяем принадлежность краев отверстия стене
                var lengthRight = (nextPosition - (holPosition + holDelta)).magnitude;
                var lengthLeft = (currentPosition - (holPosition - holDelta)).magnitude;
                if (lengthRight > Length || lengthLeft > Length)
                {
                    // Отверстие не пренадлежит стене
                }
                else
                {
                    // Вертикальные срезы
                    var rightSideOff = (nextPosition - holPosition).magnitude <= holDelta.magnitude;
                    var leftSideOff = (currentPosition - holPosition).magnitude <= holDelta.magnitude;
                    var holeRectPosition = new Vector2((holPosition - holDelta - currentPosition).magnitude, hole.Y);
                    var holeRectSize = new Vector2(hole.Size.x, holeSizeY);

                    if (rightSideOff && leftSideOff)
                    {
                        // Отверстие выходит за стену с двух сторон
                        holeRectPosition.x = 0f;
                    }
                    else if (rightSideOff)
                    {
                        // Отверстие выходит справа
                        _grid.AddX((currentPosition - (holPosition - holDelta)).magnitude);
                    }
                    else if (leftSideOff)
                    {
                        // Отверстие выходит слева
                        holeRectSize.x -= holeRectPosition.x;
                        holeRectPosition.x = 0f;
                        _grid.AddX((currentPosition - (holPosition + holDelta)).magnitude);
                    }
                    else
                    {
                        // Отверстие в центре
                        _grid.AddX((currentPosition - (holPosition - holDelta)).magnitude);
                        _grid.AddX((currentPosition - (holPosition + holDelta)).magnitude);
                    }

                    // Горизонтальные срезы
                    var upSideOff = height <= hole.Y + holeSizeY;
                    var downSideOff = hole.Y <= 0f;

                    if (upSideOff && downSideOff)
                    {
                        // Отверстие выходит за стену сверху и снизу
                    }
                    else if (upSideOff)
                    {
                        // Отверстие выходит вверх
                        _grid.AddY(hole.Y);
                    }
                    else if (downSideOff)
                    {
                        // Отверстие выходит вниз
                        _grid.AddY(hole.Y + holeSizeY);
                    }
                    else
                    {
                        // Отверстие в центре
                        _grid.AddY(hole.Y);
                        _grid.AddY(hole.Y + holeSizeY);
                    }

                    var holeRect = new Rect(holeRectPosition, holeRectSize);
                    gridHoles.Add(holeRect);
                }
            }
        }

        VertexPoints = _grid.Calculate(gridHoles, currentPosition, nextPosition);
    }

    public int GetVertexCount()
    {
        return _grid.XSize * _grid.YSize;
    }

    public int GetMainTrianglesCount()
    {
        return (_grid.XSize - 1) * (_grid.YSize - 1) * 6 - _grid.DeleteMainTriangles;
    }
}

public struct MeshPoint2
{
    public Vector3 Position;
    public Vector2 Uv;
    public bool Hole;

    public MeshPoint2(Vector3 position, Vector2 uv, bool hole)
    {
        Position = position;
        Uv = uv;
        Hole = hole;
    }
}