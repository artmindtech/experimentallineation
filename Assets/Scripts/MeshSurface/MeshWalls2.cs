﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class MeshWalls2 : MonoBehaviour
{
    public WallPoint[] WallPoints;
    public Material SecondaryMaterial;
    [SerializeField] private MeshFilter meshFilter;
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private SurfacesView[] surfaces;
    private Mesh _mesh;
    private Vector3[] _meshVertices;
    private Vector2[] _meshUv;
    private int[][] _meshTrianglesMain;
    private int _vertexesCount;
    private float _uLength;
    private bool _waitDraw;

    // Start is called before the first frame update
    public void OnValidate()
    {
        meshFilter = GetComponent<MeshFilter>();
        if (_mesh == null)
        {
            _mesh = new Mesh();
            _mesh.name = "GenerateWall";
        }
        else
        {
            _mesh.Clear();
        }

        meshRenderer = meshRenderer ?? GetComponent<MeshRenderer>();
        if (!_waitDraw)
        {
            _waitDraw = true;
            Invoke("DrawMesh", 0.1f);
        }
    }

    private void Awake()
    {
        _mesh = new Mesh();
        _mesh.name = "GenerateWall";
        meshFilter.mesh = _mesh;
        DrawMesh();
        Invoke("TestHide", 2.1f);
        Invoke("TestShow", 4.1f);
        Invoke("TestHide", 6.1f);
        Invoke("TestShow", 6.3f);
    }

    private void TestShow()
    {
        for (int i = 0; i < surfaces.Length; i++)
        {
            surfaces[i].Show();
        }
    }
    
    private void TestHide()
    {
        for (int i = 0; i < surfaces.Length; i++)
        {
            surfaces[i].Hide();
        }
    }
    
    private void DrawMesh()
    {
        _waitDraw = false;
        var count = WallPoints.Length;

        _vertexesCount = 0;
        _uLength = 0;
        _meshTrianglesMain = new int[count][];

        var meshesData = new MeshData2[WallPoints.Length];
        var allMaterials = new Material[count + 1];
        var countVertexes = 0;
        var countSecondaryTriangles = 0;

        if (surfaces != null && surfaces.Length > 0)
        {
            while (transform.childCount > 0)
            {
#if UNITY_EDITOR
                DestroyImmediate(transform.GetChild(0).gameObject);
#else
                Destroy(transform.GetChild(0).gameObject);
#endif
            }
        }

        surfaces = new SurfacesView[count];

        for (int i = 0; i < count; i++)
        {
            var nextPoint = WallPoints[0];
            if (i + 1 < count)
                nextPoint = WallPoints[i + 1];

            MeshData2 data = new MeshData2();
            var height = 3f;

            data.Init(WallPoints[i], nextPoint.Position, height);
            countVertexes += data.GetVertexCount();
            _meshTrianglesMain[i] = new int[data.GetMainTrianglesCount()];
            meshesData[i] = data;
            allMaterials[i] = WallPoints[i].Material;

            var view = new GameObject($"SurfaceView_{i}", typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider),
                typeof(SurfacesView));
            view.transform.SetParent(transform);
            surfaces[i] = view.GetComponent<SurfacesView>();
            surfaces[i].Init(data, WallPoints[i].Material);
        }
    }
}