using UnityEngine;

public class SelectSurface : MonoBehaviour
{
    public bool Enable = true;
    public Vector3 TargetPoint = Vector3.zero;
    private RaycastHit _hit;
    private Camera _camera;
    // private SelectWallSurface _selectedWall;

    // private class SelectWallSurface
    // {
    //     public MeshWallsView MeshWallsView { get; }
    //     public int Index { get; }
    //     public Vector3 Center { get; }
    //
    //     public SelectWallSurface(MeshWallsView meshWallsView, int index, Vector3 center)
    //     {
    //         MeshWallsView = meshWallsView;
    //         Index = index;
    //         Center = center;
    //     }
    // }

    void Start()
    {
        _camera = GetComponent<Camera>();
    }

    private void LateUpdate()
    {
        if (!Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out _hit))
            return;

        if (_hit.collider.gameObject.TryGetComponent(out SurfacesView view))
        {
            MeshCollider meshCollider = _hit.collider as MeshCollider;
            if (meshCollider == null || meshCollider.sharedMesh == null)
                return;

            Debug.LogWarning($"SelectSurface <color=cyan>LateUpdate</color> meshCollider.gameObject.name:{meshCollider.gameObject.name}");
            
            // Mesh mesh = meshCollider.sharedMesh;
            // Vector3[] vertices = mesh.vertices;
            // int[] triangles = mesh.triangles;
            // Vector3 p0 = vertices[triangles[_hit.triangleIndex * 3 + 0]];
            // Vector3 p1 = vertices[triangles[_hit.triangleIndex * 3 + 1]];
            // Vector3 p2 = vertices[triangles[_hit.triangleIndex * 3 + 2]];
            // Transform hitTransform = _hit.collider.transform;
            // p0 = hitTransform.TransformPoint(p0);
            // p1 = hitTransform.TransformPoint(p1);
            // p2 = hitTransform.TransformPoint(p2);
            // Debug.DrawLine(p0, p1);
            // Debug.DrawLine(p1, p2);
            // Debug.DrawLine(p2, p0);
            // TargetPoint = p0;
            // DeselectWall();
            // var center = (p0 + p1 + p2) / 3f;
            // _selectedWall = new SelectWallSurface(view,  GetSubMeshIndex(mesh, _hit.triangleIndex), center);
            // view.SelectSurface(_selectedWall.Index);
        }
        else
        {
            DeselectWall();
        }
    }

    private void DeselectWall()
    {
        // if (_selectedWall != null)
        // {
        //     _selectedWall.MeshWallsView.DeselectSurface(_selectedWall.Index);
        //     _selectedWall = null;
        // }
    }
    // private void Update()
    // {
    //     if (!Enable)
    //         return;
    //     var ray = new Ray(transform.position, transform.forward * 100f);
    //     if (Physics.Raycast(ray, out _result))
    //     {
    //         TargetPoint = _result.point;
    //         Debug.LogWarning($"WallSelectorCamera <color=cyan>Update</color> TargetPoint:{TargetPoint}");
    //     }
    // }

    public static int GetSubMeshIndex(Mesh mesh, int triangleIndex)
    {
        if (mesh.isReadable == false)
        {
            Debug.LogError("You need to mark model's mesh as Read/Write Enabled in Import Settings.", mesh);
            return 0;
        }

        int triangleCounter = 0;
        for (int subMeshIndex = 0; subMeshIndex < mesh.subMeshCount; subMeshIndex++)
        {
            var indexCount = mesh.GetSubMesh(subMeshIndex).indexCount;
            triangleCounter += indexCount / 3;
            if (triangleIndex < triangleCounter)
            {
                return subMeshIndex;
            }
        }

        Debug.LogError(
            $"Failed to find triangle with index {triangleIndex} in mesh '{mesh.name}'. Total triangle count: {triangleCounter}",
            mesh);
        return 0;
    }

    private void OnDrawGizmos()
    {
        if (!Enable)
            return;
        Gizmos.DrawSphere(TargetPoint, 0.2f);
    }

    // public bool HasSelected()
    // {
    //     return _selectedWall != null;
    // }
    // public Vector3 GetSelectedCenter()
    // {
    //     return _selectedWall.Center;
    // }
    //
    // public bool SelectedCenterPoint()
    // {
    //     
    //     return _selectedWall != null;
}

