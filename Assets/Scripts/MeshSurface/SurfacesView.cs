using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

[ExecuteAlways]
public class SurfacesView : MonoBehaviour
{
    private MeshRenderer _renderer;
    private MeshFilter _filter;
    private BoxCollider _collider;
    private Vector3[] _meshVertices;
    private Vector2[] _meshUv;
    private int[] _meshTrianglesMain;
    private Mesh _mesh;
    private Material _material;
    private TweenerCore<Color, Color, ColorOptions> _animation;

    private void Awake()
    {
        _renderer = GetComponent<MeshRenderer>();
        _filter = GetComponent<MeshFilter>();
        _collider = GetComponent<BoxCollider>();
        if (_mesh != null)
        {
            SetViewSurface();
        }
    }

    private void SetViewSurface()
    {
        _material.SetShaderPassEnabled("ShadowCaster", true);
        _renderer.sharedMaterial = _material;
        _filter.mesh = _mesh;
        // _collider.sharedMesh = _mesh;
        // _collider.convex = true;
    }

    private void Generate(MeshData2 date)
    {
        MeshPoint2[][] points = date.VertexPoints;
        int ySize = points.Length - 1;
        int xSize = points[0].Length - 1;
        float length = date.Length;
        int trianglesCount = 0;
        var holesCount = 0;

        int _vertexesCount = 0;
        int _uLength = 0;

        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                var meshPoint = points[y][x];
                _meshVertices[_vertexesCount + i] = meshPoint.Position;
                _meshUv[_vertexesCount + i] = new Vector2(_uLength + meshPoint.Uv.x, meshPoint.Uv.y);
            }
        }


        int ti = 0, vi = 0;
        for (int y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                var meshPoint = points[y][x];
                if (meshPoint.Hole)
                {
                    holesCount += 6;
                    continue;
                }

                _meshTrianglesMain[trianglesCount] = _vertexesCount + vi;
                _meshTrianglesMain[trianglesCount + 1] =
                    _meshTrianglesMain[trianglesCount + 4] = _vertexesCount + vi + xSize + 1;
                _meshTrianglesMain[trianglesCount + 2] =
                    _meshTrianglesMain[trianglesCount + 3] = _vertexesCount + vi + 1;
                _meshTrianglesMain[trianglesCount + 5] = _vertexesCount + vi + xSize + 2;
                trianglesCount += 6;
                // 
            }
        }
    }

    public void Show()
    {
        if (!IsInit())
            return;
        ClearAnimation();
        if (_renderer.sharedMaterial != null && _renderer.sharedMaterial.HasProperty("_Color"))
        {
            var color = _renderer.sharedMaterial.color;
            color.a = 1f;
            _animation = _renderer.sharedMaterial.DOColor(color, 0.5f)
                .OnComplete(() => { _animation = null; });
        }
    }

    private bool IsInit()
    {
        return _renderer != null;
    }

    public void Hide()
    {
        if (!IsInit())
            return;
        ClearAnimation();
        if (_renderer.sharedMaterial != null && _renderer.sharedMaterial.HasProperty("_Color"))
        {
            var color = _renderer.sharedMaterial.color;
            color.a = 0f;
            _animation = _renderer.sharedMaterial.DOColor(color, 0.5f)
                .OnComplete(() => { _animation = null; });
        }
    }

    private void ClearAnimation()
    {
        if (_animation != null)
        {
            DOTween.Kill(_animation);
            _animation = null;
        }
    }

    public void Init(MeshData2 data, Material material)
    {
        _material = material;
        var vertexCount = data.GetVertexCount();
        _meshVertices = new Vector3[vertexCount];
        _meshUv = new Vector2[vertexCount];
        _meshTrianglesMain = new int[data.GetMainTrianglesCount() * 2];

        Generate(data);

        _mesh = new Mesh();
        _mesh.vertices = _meshVertices;
        _mesh.uv = _meshUv;
        _mesh.subMeshCount = 1;

        _mesh.SetTriangles(_meshTrianglesMain, 0);
        _mesh.RecalculateNormals();
        _mesh.RecalculateTangents();
        _mesh.RecalculateBounds();

        if (_filter != null)
        {
            SetViewSurface();
        }
    }
}