using System;
using Models.Rooms;
using UnityEngine;

[Serializable]
public class WallLine : SmartLine
{
    public const float WallLengthOffset = 0.005f; // 50 mm
    public const float WallParallelOffset = 0.0001f;
    public int Face;
    public IWall Wall;
    public bool NeedWall = false;
    public Figure Figure;
    // private LineRect _rect;
    // public Vector2 A => _a;
    // public Vector2 B => _b;
    // public LineRect Rect => _rect;

    public WallLine(IWall wall, int face, Vector2 a, Vector2 b) : base(a, b)
    {
        Wall = wall;
        Face = face;
        // _a = a;
        // _b = b;
        // _rect = new LineRect(A.x, A.y, B.x, B.y);
    }

    public void FixedParallel()
    {
        var a = Rect.Angle + Mathf.PI / 2;
        var d = WallParallelOffset;
        SetAB(new Vector2(A.x + Mathf.Cos(a) * d, A.y + Mathf.Sin(a) * d),
            new Vector2(B.x + Mathf.Cos(a) * d, B.y + Mathf.Sin(a) * d));
    }

    public override string ToString()
    {
        return base.ToString() + " a : " + A + " b : " + B;
    }

    public void Flip()
    {
        SetAB(B, A);
        // _rect = new LineRect(A.x, A.y, B.x, B.y);
    }

    public WallLine Clone()
    {
        return new WallLine(Wall, Face, A, B) { NeedWall = NeedWall };
    }
}

public interface IWall
{
}